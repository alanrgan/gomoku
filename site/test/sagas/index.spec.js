import { call, take, race } from 'redux-saga/effects';
import { asEffect } from 'redux-saga/utils';
import { channel } from 'redux-saga';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';

import metaSaga, { initializeGame, startGame, rematch, startGameWithBoard } from '../../src/sagas';
import { watchSocket } from '../../src/sagas/socket';
import { getPlayers } from '../../src/selectors';

import { FORFEIT, REMATCH, startGame as start } from '../../src/modules/game';
import { setPlayers } from '../../src/modules/players';
import { CREATE_GAME, CONNECT_TO_GAME } from '../../src/modules/socket';
import { createBoard } from '../../src/modules/board';
import { finalizeForm, FINALIZE_FORM } from '../../src/modules/game-form';

import AlphaBetaAgent from '../../src/agent/alphabeta';
import { ActionCreators } from 'redux-undo';

describe('metaSaga', () => {
  const players = {
    1: { name: 'foo' },
    2: { name: 'bar' }
  };

  const state = {
    game: { players }
  };

  const gameConfig = {
    players,
    aiConfig: {
      isAi: {
        1: false,
        2: false
      }
    },
    board: ['foo', 'bar']
  };

  // Names of the effects captured in each race in the saga.
  // Order matters here
  const raceEffectNames = {
    init: ['local', 'createGame', 'connectGame'],
    play: ['play', 'start', 'forfeit'],
    end: ['restart', 'rematch']
  };

  let initialGameRace = {
    local: take(FINALIZE_FORM),
    createGame: take(CREATE_GAME),
    connectGame: take(CONNECT_TO_GAME)
  };

  let saga;

  const provideRace = (expectedKeys, retVal) => (config, next) => {
    if (isExpectedRace(config, expectedKeys)) {
      return retVal;
    }
    return next();
  };

  const provideCall = (fnToMock, retVal) => ({ fn }, next) => fn === fnToMock ? retVal : next();

  const createBaseSaga = (providerOpts = []) => {
    saga = expectSaga(metaSaga)
      .provide([
        // Custom provider options need to be added before others
        // since they are evaluated from top to bottom
        ...providerOpts,
        // Mock out evaluation of the races
        { race: provideRace(raceEffectNames.init, { local: gameConfig }) },
        { race: provideRace(raceEffectNames.play, { start: undefined }) },
        { race: provideRace(raceEffectNames.end, { rematch: undefined }) }
      ])
      .race(initialGameRace)
      .withState(state)
      .race({
        play: call(startGame, players),
        start: call(initializeGame),
        forfeit: take(FORFEIT)
      });
  };

  describe('local gameplay', () => {
    beforeEach(() => {
      createBaseSaga();
      saga = saga.call(initializeGame, gameConfig);
    });
    
    it('passes base assertions', async () => await saga.silentRun(20));

    it('does not wait for restart or rematch if game is restarted', async () => {
      createBaseSaga([
        { race: provideRace(raceEffectNames.play, { start: undefined }) }
      ]);

      await saga
        .not.race({
          restart: race(initialGameRace),
          rematch: call(rematch)
        })
        .silentRun(20);
    });

    it('waits for restart if game has ended normally', async () => {
      createBaseSaga([
        { race: provideRace(raceEffectNames.play, { play: 'foo' }) }
      ])
      await saga
        .race({
          restart: race(initialGameRace),
          rematch: call(rematch)
        })
        .silentRun(20);
    });

    it('waits for restart if game has been forfeited', async () => {
      createBaseSaga([
        { race: provideRace(raceEffectNames.play, { forfeit: 'bar' }) }
      ]);

      await saga
        .race({
          restart: race(initialGameRace),
          rematch: call(rematch)
        })
        .silentRun(20);
    });
  });

  describe('online gameplay', () => {
    const fakeChannel = {
      flush() {},
      take() {},
      close() {}
    };

    const providers = [
      { call: provideCall(channel, fakeChannel) },
      { take: ({ channel: chan }, next) => chan === fakeChannel ? null : next() },
      [matchers.fork.fn(watchSocket), null],
    ];

    it('sets up the socket connection on create game', async () => {
      createBaseSaga([
        { race: provideRace(raceEffectNames.init, { createGame: 'foo' }) },
        ...providers
      ]);

      await saga
        .call(channel)
        .fork(watchSocket, fakeChannel, 'foo')
        .take(fakeChannel)
        .silentRun(20);
    });
    
    it('sets up the socket conncetion on connect game', async () => {
      createBaseSaga([
        { race: provideRace(raceEffectNames.init, { connectGame: 'foo' }) },
        ...providers
      ]);

      await saga
        .call(channel)
        .fork(watchSocket, fakeChannel, 'foo')
        .take(fakeChannel)
        .silentRun(20);
    });
  });
});

describe('initializeGame()', () => {
  let aiConfig;
  const players = {
    1: { name: 'foo' },
    2: { name: 'bar' }
  };
  const board = ['foo', 'bar'];

  describe('without AI', () => {
    beforeEach(() => {
      aiConfig = {
        isAi: { 1: false, 2: false }
      };
    });

    it('initializes the game correctly', async () => {
      await expectSaga(initializeGame)
        .dispatch(finalizeForm(players, board, aiConfig))
        .put(setPlayers(players))
        .put(createBoard(board))
        .put(ActionCreators.clearHistory())
        .put(start())
        .run();
    });
  });

  describe('with AI', () => {
    beforeEach(() => {
      aiConfig = {
        isAi: { 1: false, 2: true }
      };
    });

    it('attaches an AI object to the player object', async () => {
      const { effects } = await expectSaga(initializeGame)
        .dispatch(finalizeForm(players, board, aiConfig))
        .run();
      
      const putPlayers = asEffect.put(effects.put[0]);
      const attachedPlayers = putPlayers.action.players;
      expect(attachedPlayers[2]).toHaveProperty('agent', expect.any(AlphaBetaAgent));
      expect(attachedPlayers[2]).toHaveProperty('makeMove');
      expect(typeof attachedPlayers[2].makeMove).toEqual('function');
    });
  });
});

describe('rematch', () => {
  let board;
  beforeEach(() => {
    board = {
      cells: [3,4,5],
      size: 3
    }
  });

  it('runs appropriately', async () => {
    const expectedBoard = { size: 3};

    await expectSaga(rematch)
      .withState({ game: { board: { present: board } }})
      .dispatch({ type: REMATCH })
      .call(startGameWithBoard, expectedBoard)
      .run();
  });
});