import { call } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import isEqual from 'lodash/isEqual';

import { ActionTypes } from 'redux-undo';

import boardStates, { inProgressStates } from '../../data/board-states';
import { collect } from '../../../src/util/chain-utils';
import { TOGGLE_TURN, SET_GAME_STATE, STATE } from '../../../src/modules/game';
import { SET_PIECE, declareMove } from '../../../src/modules/board';
import { getGameState, processTurn, startGame } from '../../../src/sagas/game-loop';
import { undoMove, redoMove } from '../../../src/sagas/game-loop/undo-redo';

describe('Game loop sagas', () => {
  const players = {
    1: {
      name: 'foo',
      id: 1
    },
    2: {
      name: 'bar',
      id: 2
    }
  };

  describe('getGameState', () => {
    boardStates.forEach(({ title, board, gameState, squaresRemaining }) => {
      it(`should recognize final board state ${title}`, () => {
        const chains = collect(board);
        expect(getGameState({ squaresRemaining }, chains)).toEqual(gameState);
      });
    });
  });

  describe('startGame', () => {
    const createSaga = options => {
      let saga = expectSaga(startGame, options.players || []);
      if (options.provideRace) {
        const { run = true } = options.provideRace;
        saga = saga.provide({
          race(effects, next) {
            const baseRaceEffectKeys = ['run', 'undo', 'redo'];
            // Check if we are handling the base game race
            if (isEqual(Object.keys(effects), baseRaceEffectKeys)) {
              return { run };
            }
            
            return next();
          }
        });
      }

      saga = saga.race({
        run: call(processTurn, options.players || []),
        undo: call(undoMove),
        redo: call(redoMove)
      });

      return saga;
    };

    it('runs correctly', async () => {
      await createSaga({
          players,
          provideRace: {
            run: true
          },
        })
        .not.returns(true)
        .silentRun(20);
    });

    it('returns when game is over', async () => {
      let { board: cells, squaresRemaining } = boardStates[0];
      const board = { cells, squaresRemaining };

      await createSaga({ players })
        .withState({ game: { board: { present: board } } })
        .returns(true)
        .silentRun(20);
    });

    it('continues the loop on undo / redo', async () => {
      let { board: cells, squaresRemaining } = inProgressStates[0];
      const board = { cells, squaresRemaining };
      
      await createSaga({ players })
        .withState({ game: { board: { present: board }, turn: 1 } })
        .dispatch({ type: ActionTypes.UNDO })
        .not.put.actionType(SET_PIECE)
        .not.returns(true)
        .silentRun(20);

      await createSaga({ players })
        .withState({ game: { board: { present: board }, turn: 1 } })
        .dispatch({ type: ActionTypes.REDO })
        .not.put.actionType(SET_PIECE)
        .not.returns(true)
        .silentRun(20);
    });
  });

  describe('processTurn', () => {
    const createSaga = (players, board, turn = 1) => {
      return expectSaga(processTurn, players)
        .withState({
          game: {
            board: {
              present: board
            },
            turn
          }
        })
        .call(collect, board.cells)
        .call.like({ fn: getGameState })
        .put({ type: SET_GAME_STATE, gameState: STATE.IN_PROGRESS });
    };

    it('runs correctly on a successful move', async () => {
      let { board: cells, squaresRemaining } = inProgressStates[0];
      const board = { cells, squaresRemaining };
      const move = [3,4];

      await createSaga(players, board)
        .dispatch(declareMove(1, ...move))
        .put({ type: SET_PIECE, row: move[0], col: move[1], piece: 1 })
        .put({ type: TOGGLE_TURN })
        .returns(true)
        .silentRun(20);
    });

    it('runs correctly for an AI agent', async () => {
      let { board: cells, squaresRemaining } = inProgressStates[0];
      const board = { cells, squaresRemaining };
      const move = [3,4];

      const makeMove = jest.fn();
      const players = {
        1: {
          id: 1,
          name: 'foo'
        },
        2: {
          id: 2,
          agent: 'foo',
          makeMove
        }
      };

      await createSaga(players, board, 2)
        .provide([
          [matchers.call.fn(delay), null],
          [matchers.call.fn(makeMove), move]
        ])
        .call.like({ fn: delay })
        .call(makeMove, cells, squaresRemaining)
        .returns(true)
        .silentRun(20);
    });
  });
});