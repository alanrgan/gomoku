import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';

import SocketClient from '../../src/util/socket-client';
jest.mock('../../src/util/socket-client');

import {
  watchSocket,
  pollConnect,
  socketDispatch,
  watchSocketEvents
} from '../../src/sagas/socket';
import { initializeGame } from '../../src/sagas';

import { DISCONNECT, SOCKET_DISPATCH } from '../../src/modules/socket';

describe('Socket sagas', () => {

  let client = SocketClient.mock.instances[0];

  it('constructs a socket client', () => {
    expect(SocketClient).toHaveBeenCalledTimes(1);
  });

  describe('watchSocket', () => {
    const mockChannel = {
      flush() {},
      take() {},
      close() {}
    };

    it('runs correctly when there are no errors', async () => {
      const initialDispatch = {hello: 'world'};
      await expectSaga(watchSocket, mockChannel, initialDispatch)
        .provide([
          [matchers.call.fn(socketDispatch), null],
          [matchers.call.fn(pollConnect), null],
          [matchers.call.fn(initializeGame), null],
          [matchers.fork.fn(watchSocketEvents), null]
        ])
        .call(pollConnect)
        .call(socketDispatch, initialDispatch)
        .call(initializeGame)
        .fork.fn(watchSocketEvents)
        .put(mockChannel, { type: 'SETUP_DONE' })
        .silentRun();
    });
  });

  describe('pollConnect', () => {

  });
});