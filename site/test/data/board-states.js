import { PLAYER_ONE as p1, PLAYER_TWO as p2, EMPTY as em } from '../../src/util/board-utils';
import { STATE } from '../../src/modules/game';

function attachSquaresRemaining(boardConfig) {
  const squaresRemaining = boardConfig.board.reduce((acc, row) => {
    return acc + row.reduce((ct, piece) => ct + (piece === em ? 1 : 0), 0);
  }, 0);

  return {
    ...boardConfig,
    squaresRemaining
  };
}

export const endStates = [{
  title: 'Simple P1 Win',
  board: [
    [em, em, em, em, em, em],
    [p1, p1, p1, p1, p1, em],
    [em, em, em, em, em, em],
    [em, em, em, em, em, em],
    [em, em, em, em, em, em],
  ],
  gameState: STATE.PLAYER_ONE_WIN
}, {
  title: 'Simple P2 Win',
  board: [
    [em, em, em, em, em, em],
    [p2, p2, p2, p2, p2, em],
    [em, em, em, em, em, em],
    [em, em, em, em, em, em],
    [em, em, em, em, em, em],
  ],
  gameState: STATE.PLAYER_TWO_WIN
}, {
  title: 'Vertical Win',
  board: [
    [em, p2, em, em, em, em],
    [em, p2, p1, p1, p1, em],
    [em, p2, em, em, em, em],
    [em, p2, em, em, em, em],
    [em, p2, em, em, em, em],
  ],
  gameState: STATE.PLAYER_TWO_WIN
}, {
  title: 'Diagonal Win',
  board: [
    [p1, em, em, em, em, em],
    [em, p1, p1, p1, p1, em],
    [em, em, p1, em, em, em],
    [em, em, em, p1, em, em],
    [em, em, em, em, p1, em],
  ],
  gameState: STATE.PLAYER_ONE_WIN
}, {
  title: 'Anti-diagonal Win',
  board: [
    [em, em, em, em, p1, em],
    [em, p1, p1, p1, p1, em],
    [em, em, p1, em, em, em],
    [em, p1, em, em, em, em],
    [p1, em, em, em, em, em],
  ],
  gameState: STATE.PLAYER_ONE_WIN
}, {
  title: 'Draw',
  board: [
    [p2, p1, p1, p2, p1],
    [p1, p1, p2, p1, p1],
    [p1, p1, p2, p2, p1],
    [p2, p2, p1, p1, p2],
    [p1, p2, p2, p1, p1]
  ],
  gameState: STATE.DRAW
}, {
  title: 'Full Board Win',
  board: [
    [p1, p1, p1, p2, p1],
    [p1, p1, p2, p1, p1],
    [p1, p1, p1, p2, p1],
    [p2, p2, p1, p1, p2],
    [p1, p2, p2, p1, p1]
  ],
  gameState: STATE.PLAYER_ONE_WIN
}].map(attachSquaresRemaining);

export const inProgressStates = [{
  title: 'In Progress',
  board: [
    [p1, p1, em, em, em],
    [p2, p1, em, p1, p2],
    [em, em, em, p1, p2],
    [em, em, p2, p2, em],
    [em, p2, em, em, em]
  ],
  gameState: STATE.IN_PROGRESS
}].map(attachSquaresRemaining);

const boards = endStates.concat(inProgressStates);

export default boards;