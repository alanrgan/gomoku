import { EMPTY as em, PLAYER_ONE as p1, PLAYER_TWO as p2 } from '../../src/util/board-utils';

export default [{
  cells: [
    [em, em, em, em, em, em],
    [em, p1, p1, p1, p1, em],
    [p1, em, em, p1, p1, em],
    [p1, em, p1, em, p1, em],
    [p1, p1, em, em, em, p1]
  ],
  coords: {
    [p1]: [[0,1], [0,5], [1,0], [1,5]],
    [p2]: []
  }
}, {
  cells: [
    [em, em, em, em, p2, em],
    [em, p1, p1, p1, p2, em],
    [p1, p1, p1, p1, p2, em],
    [p1, p1, p2, em, p2, em],
    [p1, p1, em, em, em, p2]
  ],
  coords: {
    [p1]: [[0,1]],
    [p2]: [[4,4]]
  }
}, {
  cells: [
    [em, em, em, em, p2, em],
    [em, p1, p1, p1, p2, em],
    [p1, em, p1, p1, em, em],
    [p1, p1, p2, em, p2, em],
    [p1, p1, em, em, em, p2]
  ],
  coords: {
    [p1]: [],
    [p2]: []
  }
}];