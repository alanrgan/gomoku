import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import isEqual from 'lodash/isEqual';

Enzyme.configure({ adapter: new Adapter() });

// Matches a race object by its keys. Order matters
global.isExpectedRace = (raceObj, keys) => {
  return isEqual(Object.keys(raceObj), keys);
};