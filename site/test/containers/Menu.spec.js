import { SUBMIT_INTENT, CLEAR, SET_BOARD} from '../../src/modules/game-form';

import Menu from '../../src/containers/Menu';

describe('Menu container', () => {
  describe('mapDispatchToProps', () => {
    let props;

    beforeEach(() => {
      props = Menu.mapDispatchToProps();
      Menu.mockDispatch.mockClear();
    });

    it('has a hook to notify form submission', () => {
      props.onFormSubmit();
      expect(Menu.mockDispatch).toHaveBeenCalledWith({
        type: SUBMIT_INTENT,
        doSubmit: true
      });
    });

    it('has a hook to clear form', () => {
      props.onBoardRemove();
      expect(Menu.mockDispatch).toHaveBeenCalledWith({
        type: CLEAR
      });
    });

    it('has a hook for setting the board', () => {
      const board = ['abcdef','hijkl'];
      props.onBoardLoaded(board);
      expect(Menu.mockDispatch).toHaveBeenCalledWith({
        type: SET_BOARD,
        board
      });
    });
  });
})