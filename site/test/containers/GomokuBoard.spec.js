import { declareMove, DECLARE_MOVE } from '../../src/modules/board';
import { STATE } from '../../src/modules/game';
import { sendMove } from '../../src/modules/online-play';

import GomokuBoard from '../../src/containers/GomokuBoard';

jest.mock('react-redux');

describe('GomokuBoard', () => {
  describe('mapStateToProps', () => {
    let board;
    let players;

    beforeEach(() => {
      board = {past: 3, present: 'foobar', future: []};
      players = {
        1: {
          name: 'foo'
        },
        2: {
          name: 'bar'
        }
      };
    });

    it('extracts the correct game state values', () => {
      const state = {
        game: {
          board,
          players,
          turn: 1,
          gameState: 'FOO'
        },
        socket: {}
      };

      expect(GomokuBoard.mapStateToProps(state)).toMatchObject({
        board: board.present,
        players,
        turn: 1,
        gameState: 'FOO'
      });
    });

    it('extracts the correct socket state values', () => {
      const state = {
        game: {
          board,
          players,
          turn: 1,
          gameState: 'FOO'
        },
        socket: {
          gameStarted: false,
          localPlayerName: 'bar'
        }
      };

      expect(GomokuBoard.mapStateToProps(state)).toMatchObject({
        isOnline: false,
        ownPieceId: 2
      });
    });
  });

  describe('mapDispatchToProps', () => {
    let ownProps = {};
    let piece;
    let props;
    let row, col;

    beforeEach(() => {
      props = GomokuBoard.mapDispatchToProps();
      piece = 'foo';
      row = 3;
      col = 4;

      GomokuBoard.mockDispatch.mockClear()
    });

    describe('local gameplay', () => {
      beforeEach(() => {
        ownProps = { isOnline: false, gameState: STATE.IN_PROGRESS };
      });
    
      it('fires a declare move event on cell click', () => {
        props.onCellClick(piece, row, col, ownProps)();
        expect(GomokuBoard.mockDispatch).toHaveBeenCalledWith(declareMove(piece, row, col));
      });
    });

    describe('online gameplay', () => {
      beforeEach(() => {
        ownProps = { isOnline: true };
      });

      it('does not dispatch a declare move event immediately', () => {
        props.onCellClick(piece, row, col, ownProps)();
        expect(GomokuBoard.mockDispatch).not.toHaveBeenCalledWith(expect.objectContaining({
          type: DECLARE_MOVE
        }));
      });

      it('dispatches a send move event when the game is in progress', () => {
        ownProps.gameState = STATE.IN_PROGRESS;
        props.onCellClick(piece, row, col, ownProps)();
        expect(GomokuBoard.mockDispatch).toHaveBeenCalledWith(sendMove(piece, [row, col]));
      });

      it('does not dispatch a send move event when the game is not in progress', () => {
        ownProps.gameState = STATE.DRAW;
        props.onCellClick(piece, row, col, ownProps)();
        expect(GomokuBoard.mockDispatch).not.toHaveBeenCalledWith(sendMove(piece, [row, col]));
      });
    })
  });
});