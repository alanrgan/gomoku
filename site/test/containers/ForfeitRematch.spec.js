import React from 'react';
import { shallow } from 'enzyme';
import ForfeitRematch from '../../src/containers/ForfeitRematch';
import { STATE, FORFEIT, REMATCH } from '../../src/modules/game';

jest.mock('react-redux');

describe('ForfeitRematch', () => {
  describe('rendering', () => {
    let component;

    beforeEach(() => {
      component = shallow(
        <ForfeitRematch.reactComponent gameInProgress={true}/>
      );
    });

    it('renders the correct icon when game is in progress', () => {
      expect(component).toMatchSnapshot();
    });

    it('renders the correct icon when the game is not in progress', () => {
      component.setProps({ gameInProgress: false });
      expect(component).toMatchSnapshot();
    });
  });
  
  describe('mapStateToProps', () => {
    it('passes in gameInProgress prop correctly', () => {
      let props = ForfeitRematch.mapStateToProps({
        game: {
          gameState: STATE.DRAW
        }
      });

      expect(props.gameInProgress).toBe(false);

      props = ForfeitRematch.mapStateToProps({
        game: {
          gameState: STATE.IN_PROGRESS
        }
      });

      expect(props.gameInProgress).toBe(true);
    });
  });

  describe('mapDispatchToProps', () => {
    let props;
    beforeEach(() => {
      props = ForfeitRematch.mapDispatchToProps();
      ForfeitRematch.mockDispatch.mockClear();
    });

    it('provides a forfeit hook', () => {
      props.onForfeit();
      expect(ForfeitRematch.mockDispatch).toHaveBeenCalledWith({
        type: FORFEIT
      });
    });

    it('provides a rematch hook', () => {
      props.onRematch();
      expect(ForfeitRematch.mockDispatch).toHaveBeenCalledWith({
        type: REMATCH
      });
    });
  });
});