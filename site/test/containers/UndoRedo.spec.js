import React from 'react';
import { shallow } from 'enzyme';
import { ActionCreators } from 'redux-undo';
import UndoRedo from '../../src/containers/UndoRedo';
import { STATE, TOGGLE_TURN } from '../../src/modules/game';

jest.mock('react-redux');

describe('UndoRedo', () => {
  it('renders correctly', () => {
    const undoRedo = shallow(<UndoRedo.reactComponent />);
    expect(undoRedo).toMatchSnapshot();
  });

  describe('mapStateToProps', () => {
    const createState = (pastLen = 0, futureLen = 0, gameState = STATE.IN_PROGRESS) => {
      const past = [];
      const future = [];

      for (let i = 0; i < pastLen; i++) {
        past.push(i);
      }

      for (let i = 0; i < futureLen; i++) {
        future.push(i);
      }

      return {
        game: {
          board: {
            past,
            future
          },
          gameState
        }
      }
    };

    const expectStateProps = (propName, stateArgs, expectedValue, ownProps = {},) => {
      const state = createState(...stateArgs);
      const props = UndoRedo.mapStateToProps(state, ownProps);
      expect(props[propName]).toEqual(expectedValue);
    };

    it('sets canUndo to false when there are no previous moves', () => {
      expectStateProps('canUndo', [], false);
    });

    it('sets canUndo to true if there are moves to undo', () => {
      expectStateProps('canUndo', [1], true);
    });

    it('sets canUndo to false if the game is not in progress', () => {
      expectStateProps('canUndo', [1, 0, STATE.DRAW], false);
    });

    it('sets canRedo to true if there are moves to redo', () => {
      expectStateProps('canRedo', [0, 2], true);
    });

    it('sets canRedo to false if there are no moves to redo', () => {
      expectStateProps('canRedo', [], false);
    });

    it('sets canRedo to false if the game is not in progress', () => {
      expectStateProps('canRedo', [0, 2, STATE.DRAW], false);
    });

    it('sets canUndo and canRedo to false if explicitly disabled', () => {
      expectStateProps('canRedo', [], false, { disabled: true });
      expectStateProps('canUndo', [], false, { disabled: true });
    });
  });

  describe('mapDispatchToProps', () => {
    let props;

    beforeEach(() => {
      jest.spyOn(ActionCreators, 'undo').mockImplementation(() => {});
      jest.spyOn(ActionCreators, 'redo').mockImplementation(() => {});
      props = UndoRedo.mapDispatchToProps();
      UndoRedo.mockDispatch.mockClear();
    });

    it('calls ActionCreators.undo for onUndo', () => {
      props.onUndo();
      expect(ActionCreators.undo).toHaveBeenCalledTimes(1);
    });

    it('calls ActionCreators.redo for onRedo', () => {
      props.onRedo();
      expect(ActionCreators.redo).toHaveBeenCalledTimes(1);
    })
    
  });
});