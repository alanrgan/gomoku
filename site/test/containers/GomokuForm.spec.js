import { SUBMIT_INTENT, CLEAR, FINALIZE_FORM } from '../../src/modules/game-form';
import { ActionCreators } from 'redux-undo';

import GomokuForm from '../../src/containers/GomokuForm';

jest.mock('react-redux');

describe('GomokuForm', () => {
  describe('mapStateToProps', () => {
    it('extracts the right state values', () => {
      const board = 'foobar';
      const players = [3,4,5];

      const state = {
        gameForm: {
          doSubmit: true,
          board
        },
        game: { players }
      };

      expect(GomokuForm.mapStateToProps(state)).toEqual({
        doSubmit: true,
        players,
        importedBoard: board
      });
    });
  });

  describe('mapDispatchToProps', () => {
    beforeEach(() => {
      GomokuForm.mockDispatch.mockClear();
      jest.spyOn(ActionCreators, 'clearHistory').mockImplementation(() => {});
    });

    it('notifies of form intent', () => {
      const props = GomokuForm.mapDispatchToProps();
      props.notifyFormIntent(true);
      expect(GomokuForm.mockDispatch).toHaveBeenCalledWith(expect.objectContaining({
        type: SUBMIT_INTENT,
        doSubmit: true
      }));
    });

    it('clears form on mount', () => {
      const props = GomokuForm.mapDispatchToProps();
      props.onMount();
      expect(GomokuForm.mockDispatch).toHaveBeenCalledWith(expect.objectContaining({
        type: CLEAR
      }));
    });

    it('submits the form correctly', () => {
      const players = [3,4,5];
      const board = 'foobar';
      const props = GomokuForm.mapDispatchToProps();
      props.onFormCompleted({ players, board });
      expect(GomokuForm.mockDispatch).toHaveBeenCalledWith(expect.objectContaining({
        type: FINALIZE_FORM,
        players,
        board
      }));
    });
  });
});