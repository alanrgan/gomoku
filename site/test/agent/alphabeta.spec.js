import AlphaBetaAgent from '../../src/agent/alphabeta';
import Move from '../../src/util/move';
import { EMPTY as em } from '../../src/util/board-utils';

describe('AlphaBetaAgent', () => {
  let agent;

  beforeEach(() => {
    agent = new AlphaBetaAgent(1);
    agent.cells = [
      [em, em, em, em, em],
      [em, em, em, em, em],
      [em, em, em, em, em],
      [em, em, em, em, em],
      [em, em, em, em, em]
    ];
  });

  describe('.tryMove', () => {
    beforeEach(() => agent.movesRemaining = 20);
    
    it('calls the provided thunk', () => {
      const thunk = jest.fn();
      agent.tryMove([3,4], 1, thunk);
      expect(thunk).toHaveBeenCalledTimes(1);
    });

    it('temporarily sets the piece in its internal board', () => {
      agent.tryMove([3,4], 1, () => {
        expect(agent.cells[3][4]).toEqual(1);
      });
      expect(agent.cells[3][4]).toEqual(em);
    });

    it('temporarily pushes the move onto its stack', () => {
      agent.tryMove([3,4], 1, () => {
        expect(agent.moves).toHaveLength(1);
        expect(agent.moves[0] instanceof Move).toBe(true);
        expect(agent.moves[0].unwrap()).toEqual([3,4]);
      });

      expect(agent.moves).toHaveLength(0);
    });

    it('can handle strings as player ids', () => {
      agent.tryMove([3,4], '1', () => {
        expect(agent.cells[3][4]).toEqual(1);
      });
    });

    it('temporarily decrements movesRemaining', () => {
      agent.tryMove([3,4], 1, () => {
        expect(agent.movesRemaining).toEqual(19);
      });
      expect(agent.movesRemaining).toEqual(20);
    });
  });
});