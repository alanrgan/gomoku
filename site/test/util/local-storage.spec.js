import * as ls from '../../src/util/local-storage';

describe('local-storage wrapper', () => {
  beforeEach(() => {
    localStorage.getItem.mockReturnValue('{"game": {"foo": 3}}');
  });

  it('can load a serialized state from local storage', () => {
    expect(ls.loadState()).toEqual({ game: { foo: 3 } });
    expect(localStorage.getItem).toHaveBeenCalledTimes(1);
  });

  it('can serialize game state into JSON and store it', () => {
    const expectedSerialized = JSON.stringify({game: {foo: 3}});
    ls.saveState({ game: {foo: 3} });
    expect(localStorage.setItem).toHaveBeenCalledWith(ls.STATE_ID, expectedSerialized);
  });

  it('does not serialize keys other than \'game\'', () => {
    const expectedSerialized = JSON.stringify({});
    ls.saveState({ foo: 3 });
    expect(localStorage.setItem).toHaveBeenCalledWith(ls.STATE_ID, expectedSerialized);
  });

  it('returns undefined if no saved state is found', () => {
    localStorage.getItem.mockReturnValue(null);
    expect(ls.loadState()).toBeUndefined();
  });
});