import * as chutils from '../../src/util/chain-utils';
import * as butils from '../../src/util/board-utils';

describe('Chain utils', () => {
  describe('makeDefaultChainSet', () => {
    let chains;

    beforeEach(() => {
      chains = chutils.makeDefaultChainSet();
    });

    it('creates a properly formatted chain set', () => {
      expect(chains).toHaveProperty("" + butils.PLAYER_ONE);
      expect(chains).toHaveProperty("" + butils.PLAYER_TWO);

      [butils.PLAYER_ONE, butils.PLAYER_TWO].forEach(player => {
        butils.DIRECTIONS.forEach(dir => {
          expect(chains[player][dir]).toHaveLength(8);
          expect(chains[player][dir].every(set => set.size === 0)).toBe(true);
        });
      });
    });
  });
});