import * as butils from '../../src/util/board-utils';
import { endStates, inProgressStates } from '../data/board-states';
import winningCoordTest from '../data/winning-coords';
import { STATE } from '../../src/modules/game';

describe('Board utils', () => {
  const cells = [
    ['a','b','c','d','e'],
    ['f','g','h','i','j'],
    ['k','l','m','n','o']
  ];

  describe('iterate', () => {
    it('iterates through rows correctly', () => {
      const iter = butils.iterate({ cells, direction: butils.HORIZONTAL });
      for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 5; j++) {
          const { value } = iter.next();
          expect(value).toEqual({
            value: cells[i][j],
            coord: [i,j]
          });
        }
        const { value } = iter.next();
        expect(value).toEqual(butils.BLOCK_END);
      }
    });

    it('iterates through columns correctly', () => {
      const iter = butils.iterate({ cells, direction: butils.VERTICAL });
      for (let i = 0; i < 5; i++) {
        for (let j = 0; j < 3; j++) {
          const { value } = iter.next();
          expect(value).toEqual({
            value: cells[j][i],
            coord: [j, i]
          });
        }
        const { value } = iter.next();
        expect(value).toEqual(butils.BLOCK_END);
      }
    });

    it('iterates through diagonals correctly', () => {
      const iter = butils.iterate({ cells, direction: butils.DIAGONAL });
      const expectedSequences = [
        'a',
        'fb',
        'kgc',
        'lhd',
        'mie',
        'nj',
        'o'
      ].map(s => s.split(''));

      for (let seq of expectedSequences) {
        for (let char of seq) {
          const { value } = iter.next();
          expect(value).toMatchObject({ value: char });
        }
        
        const { value } = iter.next();
        expect(value).toEqual(butils.BLOCK_END);
      }
    });

    it('iterates through antidiagonals correctly', () => {
      const iter = butils.iterate({ cells, direction: butils.ANTI_DIAGONAL });
      const expectedSequences = [
        'k',
        'lf',
        'mga',
        'nhb',
        'oic',
        'jd',
        'e'
      ].map(s => s.split(''));

      expectedSequences.forEach(seq => {
        seq.forEach(char => {
          const { value } = iter.next();
          expect(value).toMatchObject({ value: char });
        });
        
        const { value } = iter.next();
        expect(value).toEqual(butils.BLOCK_END);
      });
    });

    it('omits end markers when directed to', () => {
      const iter = butils.iterate({
        cells,
        direction: butils.HORIZONTAL,
        withEndMarker: false
      });
      for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 5; j++) {
          const { value } = iter.next();
          expect(value).toEqual({
            value: cells[i][j],
            coord: [i,j]
          });
        }
      }
    });
  });
  
  describe('iterateFrom', () => {
    const testIterFrom = (description, expectedSeq, config) => {
      it(description, () => {
        const iter = butils.iterateFrom({
          cells,
          ...config
        });
  
        expectedSeq.split('').forEach(char => {
          const { value } = iter.next();
          expect(value).toMatchObject({ value: char });
        });
  
        expect(iter.next().value).toBeUndefined();
      });
    };

    const testIterFromAxis = (dirName, expectedSeq, config) => {
      testIterFrom(`iterates along the ${dirName} axis correctly`, expectedSeq, config);
    };

    testIterFromAxis('horizontal', 'ghij', {
      direction:  butils.HORIZONTAL,
      start: [1,1]
    });

    testIterFromAxis('vertical', 'gl', {
      direction:  butils.VERTICAL,
      start: [1,1]
    });

    testIterFromAxis('diagonal', 'hd', {
      direction: butils.DIAGONAL,
      start: [1,2]
    });

    testIterFromAxis('anti-diagonal', 'hn', {
      direction: butils.ANTI_DIAGONAL,
      start: [1,2]
    });

    testIterFromAxis('reversed horizontal', 'edcba', {
      direction: butils.HORIZONTAL,
      start: [0,4],
      reversed: true
    });

    testIterFrom('iterates over a fixed number of elements', 'bg', {
      start: [0,1],
      direction: butils.VERTICAL,
      n: 2
    });
  });

  describe('getWinner', () => {
    it('returns the player who won', () => {
      endStates.forEach(({ board, gameState }) => {
        if (gameState !== STATE.DRAW) {
          const expectedWinner = gameState === STATE.PLAYER_ONE_WIN ? 1 : 2;
          expect(butils.getWinner(board)).toEqual(expectedWinner);
        }
      });
    });

    it('returns null if no winner is found', () => {
      inProgressStates.forEach(({ board }) => {
        expect(butils.getWinner(board)).toBe(null);
      });
    });
  });

  describe('getEmptyCells', () => {
    // Create a list of board coords to set as empty
    const emptyConfig = {
      size: [6, 6],
      emptyCoords: [
        [2,3],
        [5,5],
        [1,2],
        [1,1],
        [3,5],
        [4,3],
        [4,0],
        [0,2]
      ]
    };
    let cells;

    beforeEach(() => {
      cells = [];
      for (let i = 0; i < emptyConfig.size[0]; i++) {
        let row = [];
        for (let j = 0; j < emptyConfig.size[1]; j++) {
          row.push(butils.PLAYER_ONE);
        }
        cells.push(row);
      }

      emptyConfig.emptyCoords.forEach(([x,y]) => {
        cells[x][y] = butils.EMPTY;
      });
    });

    it('collects all cells that do not have pieces on them', () => {
      const emptyIter = butils.getEmptyCells({ cells, direction: butils.HORIZONTAL });
      const emptyCells = new Set([...emptyIter].map(({ coord }) => JSON.stringify(coord)));

      emptyConfig.emptyCoords.forEach(coord => {
        expect(emptyCells.has(JSON.stringify(coord))).toBe(true);
      });
      expect(cells.length).toEqual(6);
      expect(cells[0].length).toEqual(6);
      expect(emptyCells.size).toEqual(emptyConfig.emptyCoords.length);
    });
  });

  describe('getWinningCoords', () => {
    it('gathers winning coordinates for players from the board', () => {
      winningCoordTest.forEach(({ cells, coords }) => {
        [butils.PLAYER_ONE, butils.PLAYER_TWO].forEach(playerId => {
          let winningCoords = butils.getWinningCoords({ cells, playerId });
          winningCoords = winningCoords.map(JSON.stringify);
          winningCoords = new Set(winningCoords);

          expect(winningCoords.size).toEqual(coords[playerId].length);
          coords[playerId].forEach(coord => {
            expect(winningCoords.has(JSON.stringify(coord)));
          });
        });
      });
    });
  });
});