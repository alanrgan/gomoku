import * as game from '../../src/modules/game';
import { newHistory } from 'redux-undo';

describe('Game', () => {
  describe('actions', () => {
    it('should create an action to toggle turns', () => {
      expect(game.toggleTurn()).toEqual({ type: game.TOGGLE_TURN });
    });

    it('should create an action to set the game state', () => {
      expect(game.setGameState(game.STATE.IN_PROGRESS)).toEqual({
        type: game.SET_GAME_STATE,
        gameState: game.STATE.IN_PROGRESS
      });
    });

    it('should create an action to start the game', () => {
      expect(game.startGame()).toEqual({ type: game.START_GAME });
    });

    it('should create an action for a rematch', () => {
      expect(game.rematch()).toEqual({ type: game.REMATCH });
    });

    it('should create an action for forfeiting', () => {
      expect(game.forfeit()).toEqual({ type: game.FORFEIT });
    });
  });

  describe('reducer', () => {
    const reducer = game.default;
    let initialBoard;

    beforeEach(() => initialBoard = newHistory([], undefined, []));

    it('should return the appropriate initial state', () => {
      expect(reducer(undefined, {})).toMatchObject({
        turn: 1,
        gameState: game.STATE.IDLE,
        board: expect.objectContaining({
          past: [],
          future: []
        })
      });
    });

    describe('START_GAME', () => {
      const initialState = {
        gameState: game.STATE.DRAW,
        turn: 2,
        board: initialBoard
      };

      it('should reset the game to initial game values', () => {
        expect(reducer(initialState, { type: game.START_GAME })).toMatchObject({
          turn: 1,
          gameState: game.STATE.IN_PROGRESS
        });
      });
    });

    describe('TOGGLE_TURN', () => {
      let action;

      beforeEach(() => action = { type: game.TOGGLE_TURN });

      it('should toggle turn if the state is IN_PROGRESS', () => {
        expect(reducer({
          turn: 1,
          gameState: game.STATE.IN_PROGRESS,
          board: initialBoard
        }, action)).toMatchObject({ turn: 2 });

        expect(reducer({
          turn: 2,
          gameState: game.STATE.IN_PROGRESS,
          board: initialBoard
        }, action)).toMatchObject({ turn: 1 });
        
      });

      it('should not toggle turn if state is not IN_PROGRESS', () => {
        ['DRAW', 'PLAYER_ONE_WIN', 'PLAYER_TWO_WIN'].forEach(state => {
          expect(reducer({
            turn: 1,
            gameState: game.STATE[state],
            board: initialBoard
          }, action)).toMatchObject({ turn: 1 });
        });
      });
    });

    describe('SET_GAME_STATE', () => {
      let initialState = {
        gameState: game.STATE.IN_PROGRESS,
        board: initialBoard
      };

      it('should set the game state appropriately', () => {
        expect(reducer(initialState, game.setGameState(game.STATE.DRAW))).toMatchObject({
          gameState: game.STATE.DRAW
        });
      });
    });

    describe('FORFEIT', () => {
      it('should handle forfeiting correctly', () => {
        const initialState = {
          turn: 1,
          board: initialBoard
        };

        expect(reducer(initialState, game.forfeit())).toMatchObject({
          gameState: game.STATE.PLAYER_TWO_WIN
        });

        initialState.turn = 2;

        expect(reducer(initialState, game.forfeit())).toMatchObject({
          gameState: game.STATE.PLAYER_ONE_WIN
        });
      });
    });
  });
});