import * as socket from '../../src/modules/socket';

describe('Socket module', () => {
  describe('actions', () => {
    it('should create an action for connecting to a server', () => {
      expect(socket.connect()).toEqual({ type: socket.CONNECT });
    });

    it('should create an action for disconnecting from a server', () => {
      expect(socket.disconnect()).toEqual({ type: socket.DISCONNECT });
    });

    it('should create an action for connecting to a game', () => {
      const playerId = 'hello';
      const gameId = 'abcdef';
      expect(socket.connectToGame(playerId, gameId)).toEqual({
        type: socket.CONNECT_TO_GAME,
        playerId,
        gameId
      });
    });

    it('should create an action for creating a game', () => {
      const playerId = 'hello';
      expect(socket.createGame(playerId)).toEqual({
        type: socket.CREATE_GAME,
        playerId
      });
    });

    it('should create an action for notifying of a game being created', () => {
      const gameId = 'abcdefg';
      expect(socket.notify.gameCreated(gameId)).toEqual({
        type: socket.NOTIFY.GAME.CREATED,
        gameId
      });
    });

    it('should create an action for notifying game connecting', () => {
      const gameId = 'abcdefgh';
      const players = ['foo','bar'];
      expect(socket.notify.gameConnected(gameId, players)).toEqual({
        type: socket.NOTIFY.GAME.CONNECTED,
        gameId,
        players
      });
    });

    it('should create an action to notify of a player disconnecting', () => {
      const player = 'foobar';
      expect(socket.notify.playerDisconnected(player)).toEqual({
        type: socket.NOTIFY.PLAYER.DISCONNECTED,
        player
      });
    });
  });

  describe('reducer', () => {
    const reducer = socket.default;

    it('returns the appropriate initial state', () => {
      expect(reducer()).toMatchObject({
        error: null,
        players: null,
        connected: false,
        gameId: null
      });
    });

    it('sets connected state to true on connect success', () => {
      const action = { type: socket.CONNECT_SUCCESS };
      const state = { connected: false };
      expect(reducer(state, action)).toMatchObject({
        connected: true
      });
    });

    it('sets connected state to false on disconnect success', () => {
      const action = { type: socket.DISCONNECT_SUCCESS };
      const state = { connected: true };
      expect(reducer(state, action)).toMatchObject({
        connected: false
      });
    });

    it('sets game id on a game created notification', () => {
      const state = { gameId: null };
      const gameId = 'abcdefg';
      const action = socket.notify.gameCreated(gameId);

      expect(reducer(state, action)).toMatchObject({ gameId });
    });

    it('sets game id and players on game connected notification', () => {
      const state = { gameId: null, players: null };
      const gameId = 'abcdefggh';
      const players = ['alpha', 'beta'];
      const action = socket.notify.gameConnected(gameId, players);
      expect(reducer(state, action)).toMatchObject({ gameId, players });
    });
    
    it('filters out disconnected players on a disconnect notification', () => {
      let players = ['foo','bar','baz','quux'];
      let state = { players };
      const action = socket.notify.playerDisconnected('quux');
      expect(reducer(state, action)).toMatchObject({
        players: ['foo', 'bar', 'baz']
      });

      players = null;
      state = { players };
      expect(reducer(state, action)).toMatchObject({
        players: []
      });
    });
  });
});