import * as Board from '../../src/modules/board';
import { newHistory } from 'redux-undo';

describe('Board module', () => {
  describe('actions', () => {
    it('should create an action to create a board', () => {
      const board = [['hello'],['world']];
      const expectedAction = {
        type: Board.CREATE,
        board
      };

      expect(Board.createBoard(board)).toEqual(expectedAction);
    });

    it('should create an action to set a piece at a position', () => {
      const row = 3;
      const col = 5;
      const piece = 2;
      const expectedAction = {
        type: Board.SET_PIECE,
        row,
        col,
        piece
      };

      expect(Board.setPieceAt(piece, row, col)).toEqual(expectedAction);
    });
  });
  
  describe('reducer', () => {
    const { reducer } = Board;
    let initialCells;

    const generateCells = (rows, cols) => {
      const cells = [];
      for (let i = 0; i < rows; i++) {
        const row = [];
        for (let j = 0; j < cols; j++) {
          row.push(null);
        }
        cells.push(row);
      }
      return cells;
    };
    
    beforeEach(() => {
      initialCells = generateCells(7, 7);
    });

    it('should return the initial state', () => {
      expect(reducer(undefined, {})).toEqual({
        cells: initialCells,
        size: {
          rows: 7,
          columns: 7
        },
        maxDims: {
          rows: 10,
          columns: 10
        },
        squaresRemaining: 7 * 7
      });
    });

    describe('CREATE action', () => {
      it('should do nothing if the dimensions are invalid', () => {
        const state = {
          cells: [3,4,5]
        };

        const invalidDims = [
          { rows: 0, columns: 5 },
          { rows: 5, columns: 0 },
          { rows: 11, columns: 6 },
          { rows: 6, columns: 11 }
        ];

        invalidDims.forEach(dim => {
          expect(reducer(state, Board.createBoard({
            size: dim
          }))).toEqual(state);
        });
      });

      it('should set the board if cells are provided', () => {
        const action = {
          type: Board.CREATE,
          board: {
            cells: initialCells,
            size: {
              rows: 5,
              columns: 5
            }
          }
        };
        
        expect(reducer({
          cells: [],
          size: {
            rows: 5,
            columns: 5
          },
          maxDims: {
            rows: 10,
            columns: 10
          }
        }, action)).toMatchObject({
          cells: initialCells,
          size: {
            rows: 5,
            columns: 5
          },
          maxDims: {
            rows: 10,
            columns: 10
          },
          squaresRemaining: 5 * 5
        });
      });

      it('should generate a board if cells are not provided', () => {
        const action = {
          type: Board.CREATE,
          board: {
            size: {
              rows: 8,
              columns: 5
            }
          }
        };

        expect(reducer({
          cells: [],
          size: { rows: 5, columns: 5 }
        }, action)).toMatchObject({
          cells: generateCells(8, 5),
          size: { rows: 8, columns: 5 },
          squaresRemaining: 8 * 5
        });
      });
    });

    describe('SET_PIECE action', () => {
      it('should do nothing if the game is at an end state', () => {
        const action = { type: Board.SET_PIECE, row: 1, col: 1, piece: 2 };
        const state = { cells: [2,3,4] };
        expect(reducer(state, action, true)).toEqual(state);
      });

      it('should do nothing if the specified move is invalid', () => {
        const state = {
          size: {
            rows: 5,
            columns: 5,
          },
          cells: Board.generateBoard(5, 5)
        };
        
        const invalidMoves = [
          [6, 3],
          [-1, 3],
          [3, 5],
          [3, -1]
        ];

        invalidMoves.forEach(move => {
          expect(reducer(state, Board.setPieceAt(1, ...move))).toEqual(state);
        });
      });

      it('should set the piece and decrement squaresRemaining', () => {
        const state = {
          size: {
            rows: 5,
            columns: 5
          },
          cells: Board.generateBoard(5, 5),
          squaresRemaining: 25
        };

        const expectedState = {
          ...state,
          cells: state.cells.concat(),
          squaresRemaining: 24
        };

        expectedState.cells[1][1] = 1;

        expect(reducer(state, Board.setPieceAt(1, 1, 1))).toMatchObject(expectedState)
      });
    });

    describe('undoable', () => {
      const undoable = Board.default;
      let state;

      const currBoard = { cells: [[3,4,6,5],[3,4,5,6]], size: { rows: 2, columns: 4 } };

      beforeEach(() => {
        state = newHistory([], currBoard, []);
      });

      it('should ignore redux-internal actions (^@@)', () => {
        expect(undoable(state, { type: '@@redux/INIT '})).toEqual(state);
      });

      it('should modify history with actions that produce a distinct state', () => {
        expect(undoable(state, Board.setPieceAt(1, 0, 1))).toMatchObject({
          past: [currBoard]
        });
      });
    });
  });
})