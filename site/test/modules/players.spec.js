import * as players from '../../src/modules/players';

describe('Players module', () => {
  describe('actions', () => {
    it('should create an action for setting players', () => {
      const ps = {1: 'foo', 2: 'bar'};
      const expectedAction = {
        type: players.SET,
        players: ps
      };
      expect(players.setPlayers(ps)).toEqual(expectedAction);
    });
  });

  describe('reducer', () => {
    const reducer = players.default;

    it('should return the initial state', () => {
      expect(reducer(undefined, {})).toEqual({
        1: {
          color: 'red'
        },
        2: {
          color: 'blue'
        }
      });
    });

    it('should handle SET actions by merging', () => {
      const action = {
        type: players.SET,
        players: {
          1: { name: 'bar' },
          2: { color: '#fea221', name: 'zap' }
        }
      };

      expect(reducer({
        1: {
          color: 'red',
          name: 'foo'
        },
        2: {
          color: 'blue',
        }
      }, action)).toEqual({
        1: {
          color: 'red',
          name: 'bar'
        },
        2: {
          color: '#fea221',
          name: 'zap'
        }
      });
    });
  });

  describe('isInitialized', () => {
    it('returns true if player names have been set', () => {
      const playersObj = {
        1: { name: 'foo' },
        2: { name: 'bar' }
      };
      expect(players.isInitialized(playersObj)).toBe(true);
    });

    it('returns false if player names are empty', () => {
      let playersObj = {
        1: { name: '' },
        2: { name: 'foo' }
      };;
      expect(players.isInitialized(playersObj)).toBe(false);

      playersObj = {
        1: { name: ' ' },
        2: { name: 'foo' }
      };

      expect(players.isInitialized(playersObj)).toBe(false);

      playersObj = {
        1: { name: undefined },
        2: { name: 'foo' }
      };

      expect(players.isInitialized(playersObj)).toBe(false);
    });
  });
});