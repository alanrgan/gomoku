import * as gameForm from '../../src/modules/game-form';

describe('Game form', () => {
  describe('actions', () => {
    it('should create an action to set the imported board', () => {
      const board = [['hello'],['world']];
      const expectedAction = {
        type: gameForm.SET_BOARD,
        board
      };
      expect(gameForm.setBoard(board)).toEqual(expectedAction);
    });

    it('should create an action to clear the form', () => {
      const expectedAction = {
        type: gameForm.CLEAR
      };
      expect(gameForm.clearForm()).toEqual(expectedAction);
    });

    it('should create an action to set the game form submit intent', () => {
      const doSubmit = true;
      const expectedAction = {
        type: gameForm.SUBMIT_INTENT,
        doSubmit
      };
      expect(gameForm.setGameFormIntent(doSubmit)).toEqual(expectedAction);
    });
  });

  describe('reducer', () => {
    const reducer = gameForm.default;

    it('should return default state', () => {
      expect(reducer(undefined, {})).toEqual({});
    });

    it('should handle CLEAR actions', () => {
      expect(reducer({board: 'foo'}, { type: gameForm.CLEAR })).toEqual({});
    });

    it('should handle SET_BOARD', () => {
      const board = ['foo', 'bar'];
      expect(reducer(undefined, {
        type: gameForm.SET_BOARD,
        board
      })).toEqual({ board });
    });

    it('should handle SUBMIT_INTENT', () => {
      expect(reducer({ board: 'foo' }, {
        type: gameForm.SUBMIT_INTENT,
        doSubmit: true
      })).toEqual({ board: 'foo', doSubmit: true });
    });
  });
});