import React from 'react';
import { mount } from 'enzyme';

import ColoredBox from '../../src/components/ColoredBox';

describe('ColoredBox', () => {
  it('renders defaults correctly', () => {
    expect(mount(<ColoredBox />)).toMatchSnapshot();
  });

  it('renders correctly with shadow', () => {
    expect(mount(<ColoredBox color='#9824fa' />)).toMatchSnapshot();
  });

  it('renders correctly without shadow', () => {
    expect(mount(<ColoredBox noShadow color='#2421bd' />)).toMatchSnapshot();
  });
});