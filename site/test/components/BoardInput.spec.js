import React from 'react';
import { shallow } from 'enzyme';
import BoardInput from '../../src/components/BoardInput';

describe('BoardInput', () => {
  let component;
  let instance;
  let onBoardLoaded;
  let onBoardRemove;
  let fileEventListener;
  let readAsText;
  let fileInput;

  const testBoard = '{"foo":"bar", "baz": 3}';
  const parsedBoard = JSON.parse(testBoard);

  beforeEach(() => {
    onBoardLoaded = jest.fn();
    onBoardRemove = jest.fn();
    fileEventListener = jest.fn();
    readAsText = jest.fn();

    component = shallow(
      <BoardInput
        id='board-input'
        onBoardLoaded={onBoardLoaded} 
        onBoardRemove={onBoardRemove} />
    );

    instance = component.instance();

    window.FileReader = jest.fn(() => ({
      addEventListener: fileEventListener,
      readAsText
    }));

    fileInput = component.find('input');
    
  });

  it('renders correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('does nothing if no file is selected', () => {
    fileInput.simulate('change', {});
    expect(fileEventListener).not.toHaveBeenCalled();

    fileEventListener.mockClear();
    fileInput.simulate('change', { target: { files: [] } });
    expect(fileEventListener).not.toHaveBeenCalled();
  });

  describe('RemoveButton', () => {
    let removeButton;

    beforeEach(() => {
      fileInput.simulate('change', {
        target: {
          files: ['foo']
        }
      });

      fileEventListener.mock.calls[0][1]({ target: { result: testBoard } });
      removeButton = component.find({ variant: 'contained' });
    });

    it('triggers the event listener on input change', () => {
      expect(fileEventListener).toHaveBeenCalledTimes(1);
      expect(readAsText).toHaveBeenCalledWith('foo');
    });
  
    it('triggers onBoardLoaded callback on file upload', () => {
      fileEventListener.mock.calls[0][1].bind(instance)({ target: { result: testBoard } });
      expect(onBoardLoaded).toHaveBeenCalledWith(parsedBoard);
      expect(instance.state.imported).toBe(true);
    });

    it('renders correct button label after import', () => {
      expect(removeButton.text()).toEqual('Imported');
    });

    it('renders \'REMOVE\' label on mouse enter', () => {
      removeButton.simulate('mouseEnter');
      expect(instance.state.label).toEqual('Remove');
    });

    it('returns to original label after mouse leave', () => {
      removeButton.simulate('mouseEnter');
      removeButton.simulate('mouseLeave');
      expect(instance.state.label).toEqual('Imported');
    });

    it('removes imported board on click', () => {
      instance.fileInput = {};
      removeButton.simulate('click');
      expect(onBoardRemove).toHaveBeenCalledTimes(1);
      expect(instance.state).toMatchObject({ imported: false, label: 'Import Board' });
      expect(instance.fileInput.value).toEqual('');
    });
  });
});