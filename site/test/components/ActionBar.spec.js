import React from 'react';
import { shallow } from 'enzyme';

import ActionBar from '../../src/components/ActionBar';

describe('ActionBar', () => {
  let instance;
  let component;

  beforeEach(() => {
    component = shallow(<ActionBar />);
    instance = component.instance();
  });

  it('renders correctly', () => {
    expect(component).toMatchSnapshot();
  });
});