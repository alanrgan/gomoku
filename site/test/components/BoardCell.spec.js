import React from 'react';
import { shallow } from 'enzyme';

import BoardCell from '../../src/components/BoardCell';

describe('BoardCell', () => {
  let component;
  let onClick;

  beforeEach(() => {
    onClick = jest.fn();
    component = shallow(<BoardCell onClick={onClick} />);
  });

  it('renders correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('renders a piece with color correctly', () => {
    component = shallow(<BoardCell onClick={onClick} color='red' />);
    expect(component).toMatchSnapshot();
  });

  it('triggers onClick callback', () => {
    component.simulate('click');
    expect(onClick).toHaveBeenCalledTimes(1);
  });
});