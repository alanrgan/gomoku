import React from 'react';
import { shallow } from 'enzyme';

import { STATE } from '../../src/modules/game';
import StatusBar from '../../src/components/StatusBar';

describe('StatusBar', () => {
  let statusBar;
  const players = {
    1: {
      name: 'hello',
      color: '#db2245'
    },
    2: {
      name: 'world',
      color: '#55f219'
    }
  };
  
  const renderTest = props => {
    statusBar = shallow(<StatusBar {...props} />);
    expect(statusBar).toMatchSnapshot();
  };

  it('renders player turns correctly', () => {
    renderTest({ turn: 1, players, gameState: STATE.IN_PROGRESS });
    renderTest({ turn: 2, players, gameState: STATE.IN_PROGRESS });
  });

  it('renders player wins correctly', () => {
    ['PLAYER_ONE_WIN', 'PLAYER_TWO_WIN'].forEach(winState => {
      renderTest({ turn: 1, players, gameState: STATE[winState] });
    });
  });

  it('renders a draw correctly', () => {
    renderTest({ turn: 2, players, gameState: STATE.DRAW });
  });
});