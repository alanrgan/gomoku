import React from 'react';
import { shallow } from 'enzyme';

import CenterFlex from '../../src/components/CenterFlex';

describe('CenterFlex', () => {
  it('renders correctly', () => {
    const elem = shallow(
      <CenterFlex>
        <div>
          foo.bar
        </div>
      </CenterFlex>
    );
    
    expect(elem).toMatchSnapshot();
  });
});