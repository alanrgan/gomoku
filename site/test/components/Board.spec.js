import React from 'react';
import { shallow } from 'enzyme';

import { Board } from '../../src/components/Board';
import { generateBoard } from '../../src/modules/board';

import saveAs from 'file-saver';

jest.mock('file-saver');
jest.unmock('react-redux');

describe('Board component', () => {
  let board;
  let history;
  let onCellClick;

  beforeEach(() => {
    history = { push: jest.fn() };
    onCellClick = jest.fn();
  });

  describe('uninitialized board', () => {
    it('redirects to the home state if board is uninitialized', () => {
      board = shallow(
        <Board
          history={history}
          players={{ 1: {}, 2: {} }}
          onCellClick={onCellClick}
        />
      );

      board.instance().componentDidMount();
      expect(history.push).toHaveBeenLastCalledWith('/');
    });

    it('does not redirect if the board is initialized', () => {
      const players = {
        1: { name: 'Foo' },
        2: { name: 'Bar' }
      };

      board = shallow(
        <Board history={history} players={players} />
      );

      board.instance().componentDidMount();
      expect(history.push).not.toHaveBeenCalled();
    });
  });

  describe('initialized board', () => {
    let players;
    let gomokuBoard;
    let instance;

    beforeEach(() => {
      players = {
        1: {
          name: 'foo',
          color: 'red'
        },
        2: {
          name: 'bar',
          color: 'blue'
        }
      };

      gomokuBoard = {
        size: {
          rows: 5,
          columns: 5
        },
        cells: generateBoard(5, 5),
        squaresRemaining: 25,
        maxDims: {
          rows: 10,
          columns: 10
        }
      };

      gomokuBoard.cells[2][2] = 1;
      gomokuBoard.cells[2][3] = 2;

      board = shallow(
        <Board
          history={history}
          onCellClick={onCellClick}
          players={players}
          board={gomokuBoard}
        />
      );

      instance = board.instance();
    });

    it('renders correctly', () => {
      expect(board).toMatchSnapshot();
    });

    describe('saveBoard', () => {
      beforeEach(() => {
        saveAs.mockClear();
        window.Blob = jest.fn();
        instance.saveBoard();
      });

      it('creates a blob with the json serialized board', () => {
        expect(window.Blob).toHaveBeenCalledTimes(1);
        const params = window.Blob.mock.calls[0][0];
        expect(params).toEqual([JSON.stringify(gomokuBoard)]);
      });

      it('attempts to save the board to a file', () => {
        expect(saveAs).toHaveBeenCalledTimes(1);
      });
    });
  });
});