import React from 'react';
import { shallow } from 'enzyme';
import { TextField } from '@material-ui/core';
import { GameForm } from '../../src/components/GameForm';
import ColorPicker from '../../src/components/ColorPicker';
import DimensionSelector from '../../src/components/DimensionSelector';

describe('GameForm component', () => {
  let component;
  let instance;
  let notifyFormIntent;
  let onFormCompleted;
  let onMount;
  let doSubmit = false;
  let history;

  const setPlayerName = (pid, name) => {
    component.find(TextField).forEach((field, i) => {
      if (i === (pid - 1)) {
        const event = { target: { name: 'foo', value: name } };
        field.simulate('change', event);
      }
    });
  };

  beforeEach(() => {
    notifyFormIntent = jest.fn();
    onFormCompleted = jest.fn();
    history = { push: jest.fn() };
    onMount = jest.fn();

    component = shallow(
      <GameForm
        notifyFormIntent={notifyFormIntent}
        onFormCompleted={onFormCompleted}
        doSubmit={doSubmit}
        history={history}
        onMount={onMount}
      />
    );

    instance = component.instance();
  });

  it('renders correctly', () => {
    expect(component).toMatchSnapshot();
  });

  it('handles player input changes correctly', () => {
    component.find(TextField).forEach((field, i) => {
      const event = { target: { name: 'foo', value: `bar${i}` } };
      field.simulate('change', event);
      expect(instance.state).toHaveProperty(`players.${i+1}.name`, event.target.value);//.toMatchObject({
    });
  });

  describe('doSubmit trigger', () => {
    beforeEach(() => {
      jest.spyOn(instance, 'onFormSubmit');
    });

    it('does not submit form if inputs are invalid', () => {
      setPlayerName(1, 'foobar');
      component.setProps({ doSubmit: true });
      expect(instance.onFormSubmit).not.toHaveBeenCalled();

      expect(notifyFormIntent).toHaveBeenCalledTimes(1);
      expect(notifyFormIntent).toHaveBeenCalledWith(false);

      setPlayerName(2, '  ');
      component.setProps({ doSubmit: true });
      expect(instance.onFormSubmit).not.toHaveBeenCalled();
    });

    it('submits the form if inputs are valid', () => {
      setPlayerName(1, 'foobar');
      setPlayerName(2, 'bazquux');
      component.setProps({ doSubmit: true });
      expect(instance.onFormSubmit).toHaveBeenCalled();
    });
  });

  describe('onFormSubmit', () => {
    beforeEach(() => {
      instance.onFormSubmit();
    });

    it('resets game form intent', () => {
      expect(notifyFormIntent).toHaveBeenLastCalledWith(false);
    });

    it('triggers the form completion callback with a copy of local state', () => {
      const state = {...instance.state};
      expect(onFormCompleted).toHaveBeenCalledTimes(1);
      expect(onFormCompleted).toHaveBeenCalledWith(state);
    });

    it('includes any imported board into the final state', () => {
      const importedBoard = ['foo', 'bar', 'baz'];
      const state = {...instance.state, board: importedBoard};
      component.setProps({ importedBoard });
      instance.onFormSubmit();
      expect(onFormCompleted).toHaveBeenLastCalledWith(state);
    });

    it('redirects to the game view', () => {
      expect(history.push).toHaveBeenCalledWith('/game/');
    });
  });

  it('triggers mount callback on mount', () => {
    expect(onMount).toHaveBeenCalledTimes(1);
  });

  it('handles player color changes correctly', () => {
    component.find(ColorPicker).forEach((picker, i) => {
      const color = { hex: i };
      picker.simulate('colorChange', color);
      expect(instance.state).toHaveProperty(`players.${i+1}.color`, color.hex);
    });
  });

  it('handles board dimension changes correctly', () => {
    component.find(DimensionSelector).forEach((selector, i) => {
      const event = { target: { value: i } };
      const propName = `board.size.${selector.props().type === 'height' ? 'rows' : 'columns'}`;
      selector.simulate('change', event);
      expect(instance.state).toHaveProperty(propName, event.target.value);
    });
  });
});