import React from 'react';
import { shallow } from 'enzyme';

import { DimensionSelector } from '../../src/components';

describe('DimensionSelector', () => {
  it('renders with provided values', () => {
    const values = [3,4,6,10,12];
    const selector = shallow(<DimensionSelector id='myInput' label='foo' values={values} />);
    expect(selector).toMatchSnapshot();
  });

  it('renders with provided range', () => {
    const selector = shallow(<DimensionSelector id='myInput' label='foo' range={[3,10]} />);
    expect(selector).toMatchSnapshot();
  });
});