import React from 'react';
import { shallow } from 'enzyme';

import ColorPicker from '../../src/components/ColorPicker';

describe('ColorPicker', () => {
  it('renders correctly', () => {
    const picker = shallow(
      <ColorPicker color='red'/>
    );
    expect(picker).toMatchSnapshot();
  });
});