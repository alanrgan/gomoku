import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Menu from './containers/Menu';
import GomokuBoard from './containers/GomokuBoard';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route path='/' exact component={Menu} />
          <Route path='/game/' exact component={GomokuBoard} />
          <Route path='/g/:gameId' component={Menu} />
        </div>
      </Router>
    );
  }
}

export default App;
