export default class Agent {
  constructor(playerId) {
    this.playerId = parseInt(playerId);
    this.opponentId = this.playerId === 1 ? 2 : 1;
  }

  get id() {
    return this.playerId;
  }

  set id(value) {
    this.playerId = value;
    this.opponentId = value === 1 ? 2 : 1;
  }

  makeMove(cells, movesRemaining) {}
};