import * as chutils from '../util/chain-utils';
import * as butils from '../util/board-utils';
import * as fn from '../util/functional';
import Move from '../util/move';
import Agent from './index';
import clone from 'lodash/cloneDeep';

export default class AlphaBetaAgent extends Agent {
  static featureScores = {
    player: {
      consecutive: {
        four: [33, 170],
        three: [90, 120]
      },
      open: {
        four: 60,
        doubles: 65,
        three: 35
      },
      cross: {
        two: 10,
        three: 40
      }
    },
    opponent: {
      consecutive: {
        four: [-80, -500],
        three: [-20, -150]
      },
      open: {
        four: -120,
        three: -22,
        doubles: -60
      },
      cross: {
        two: -10,
        three: -80
      }
    }
  };

  constructor(playerId, maxDepth = 3) {
    super(playerId);
    this.cells = undefined;
    this.moves = [];
    this.maxDepth = maxDepth;
  }

  makeMove(cells, movesRemaining) {
    this.cells = clone(cells);
    this.movesRemaining = movesRemaining

    this.emptySquares = [...fn.map(({coord}) => Move.wrap(coord),
      butils.getEmptyCells({
        cells,
        direction: butils.HORIZONTAL
      })
    )];

    // Get winning coords for current player
    const winningCoords = butils.getWinningCoords({ cells, playerId: this.playerId });
    if (winningCoords.length > 0) {
      return winningCoords[0];
    }

    const opponentWinningCoords = butils.getWinningCoords({ cells, playerId: this.opponentId });
    if (opponentWinningCoords.length > 0) {
      return opponentWinningCoords[0];
    }

    const { move, score } = this.alphabeta(3, -Infinity, Infinity, true);
    console.log('move score:', score);

    return move.unwrap();
  }

  tryMove([x,y], playerId, thunk) {
    this.cells[x][y] = parseInt(playerId);
    this.moves.push(Move.wrap([x,y]));
    this.movesRemaining -= 1;
    thunk();
    this.cells[x][y] = butils.EMPTY;
    this.moves.pop();
    this.movesRemaining += 1;
  }

  calcConsecutiveScore({ chains, chainSize, playerId }) {
    const playerKey = playerId === this.playerId ? 'player' : 'opponent';
    const endSquares = butils.getEndSquares({
      cells: this.cells,
      chains,
      playerId,
      chainSize
    });

    const featureScore = AlphaBetaAgent
        .featureScores[playerKey]
        .consecutive[chainSize === 3 ? 'three' : 'four'];

    return fn.reduce((squares, acc) => (
      acc + ((squares.length === 1)
        ? featureScore[0]
        : featureScore[1])
    ), endSquares, 0);
  }

  calcSeparatedScore({ chains, playerId, shape, type }) {
    const playerKey = playerId === this.playerId ? 'player' : 'opponent';
    const separatedChains = chutils.getSeparatedChains({
      cells: this.cells,
      chains,
      playerId,
      shape
    });

    let featureScore = AlphaBetaAgent.featureScores[playerKey].open[type];
    featureScore /= type === 'doubles' ? 2 : 1;
      
    return featureScore * separatedChains.length;
  }

  calcCrossScore({ chains, playerId, crossSize }) {
    const playerKey = playerId === this.playerId ? 'player' : 'opponent';
    const crossings = chutils.findCrossings({
      cells: this.cells,
      chains,
      playerId,
      size: crossSize
    });

    const featureScore = AlphaBetaAgent.featureScores[playerKey].cross[crossSize === 2 ? 'two' : 'three'];

    return featureScore * crossings.length;
  }

  evaluate(winner) {
    const move = this.moves[this.moves.length - 1];

    if (winner !== null) {
      return winner === this.playerId
        ? { move, score: 500, winner }
        : { move, score: -500, winner }
    }

    const chains = chutils.collect(this.cells);

    let score = 0;

    const featureSets = {
      consecutive: [3,4],
      separated: [
        [2,1,'three'],
        [3,1,'four'],
        [2,2,'doubles']
      ],
      cross: [2,3]
    };

    // Calculate feature scores for this player

    featureSets.consecutive.forEach(chainSize => {
      score += this.calcConsecutiveScore({
        chains,
        playerId: this.playerId,
        chainSize
      });
    });

    featureSets.separated.forEach(([first, second, type]) => {
      score += this.calcSeparatedScore({
        chains,
        playerId: this.playerId,
        shape: { first, second },
        type
      });
    });

    featureSets.cross.forEach(crossSize => {
      score += this.calcCrossScore({
        chains,
        playerId: this.playerId,
        crossSize
      });
    });

    // Calculate scores for opponent pieces

    const oppFourConsecScore = this.calcConsecutiveScore({
      chains,
      playerId: this.opponentId,
      chainSize: 4
    });

    score += oppFourConsecScore;

    if (oppFourConsecScore === 0) {
      score += this.calcConsecutiveScore({
        chains,
        playerId: this.opponentId,
        chainSize: 3
      });

      featureSets.separated.forEach(([first, second, type]) => {
        score += this.calcSeparatedScore({
          chains,
          playerId: this.opponentId,
          shape: { first, second },
          type
        });
      });

      featureSets.cross.forEach(crossSize => {
        score += this.calcCrossScore({
          chains,
          playerId: this.opponentId,
          crossSize
        });
      });
    }

    return { move, score, winner };
  }

  clampWinByDepth(bestMove, depth) {
    switch (bestMove.winner) {
      case this.playerId:
        return { ...bestMove, score: bestMove.score - depth };
      case this.opponentId:
        return { ...bestMove, score: bestMove.score + depth };
      default:
        return bestMove;
    }
  }

  getWinner() {
    return butils.getWinner(this.cells);
  }

  alphabeta(depth, alpha, beta, isMax) {
    const winner = this.getWinner();
    if (depth === 0 || winner !== null || this.movesRemaining <= 0) {
      return this.evaluate(winner);
    }

    const emptySquares = this.emptySquares.filter(emptySq => (
      !this.moves.some(move => move.equals(emptySq))
    ));

    if (isMax) {
      let bestMove = { move: null, score: -Infinity, winner: null };
      for (let move of emptySquares) {
        let nextMove = { score: -Infinity };
        
        this.tryMove(move.unwrap(), this.playerId, () => {
          nextMove = this.alphabeta(depth - 1, alpha, beta, false);
        });

        bestMove = bestMove.score > nextMove.score ? bestMove : nextMove;

        if (bestMove.score >= beta) {
          break;
        }

        alpha = Math.max(alpha, bestMove.score);
      }

      bestMove = this.clampWinByDepth(bestMove, depth);

      return depth === this.maxDepth ? bestMove : {
        ...bestMove,
        move: this.moves[this.moves.length - 1]
      };
    } else {
      let bestMove = { move: null, score: Infinity, winner: null };
      for (let move of emptySquares) {
        let nextMove = { score: Infinity };
        this.tryMove(move.unwrap(), this.opponentId, () => {
          nextMove = this.alphabeta(depth - 1, alpha, beta, true);
        });

        bestMove = bestMove.score < nextMove.score ? bestMove : nextMove;

        if (bestMove.score <= alpha) {
          break;
        }

        beta = Math.min(beta, bestMove.score);
      }

      bestMove = this.clampWinByDepth(bestMove, depth);

      return { ...bestMove, move: this.moves[this.moves.length - 1] };
    }
  }
};