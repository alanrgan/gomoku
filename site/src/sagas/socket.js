import { call, take, put, fork, cancel, takeEvery } from 'redux-saga/effects';
import { eventChannel, delay } from 'redux-saga';
import {
  DISCONNECT,
  CREATE_GAME,
  CONNECT_TO_GAME,
  SOCKET_DISPATCH,
  socketError,
  notify
} from '../modules/socket';
import SocketClient, { RESPONSE_TYPES, ACTION_TYPES } from '../util/socket-client';
import { declareMove, SET_PIECE } from '../modules/board';
import { initializeGame } from './index';

export const host = 'ws://localhost:5555';
export const POLL_INTERVAL = 1000;
export const POLL_TIMES = 10;

const socketClient = new SocketClient(host);

/**
 * 
 * @param {channel} setupChan redux-saga channel used to notify parent saga that sockets have been set up
 * @param {*} initialDispatch initial message to send to the server
 */
export function* watchSocket(setupChan, initialDispatch) {
  try {
    // Establish a connection to the server via Websockets
    yield call(pollConnect);

    // Send initial message. Either create a new game or connect to an existing one
    yield call(socketDispatch, initialDispatch);

    // Fork two sagas. One for watching incoming socket events
    // and another for watching outgoing socket dispatches
    const watchEventTask = yield fork(watchSocketEvents);
    const watchDispatchTask = yield takeEvery(SOCKET_DISPATCH, socketDispatch);

    // Initialize game
    yield call(initializeGame);

    yield put(setupChan, { type: 'SETUP_DONE' });

    yield take(DISCONNECT);
    yield cancel(watchEventTask, watchDispatchTask);
  } catch (error) {
    console.log('error detected:', error);
    yield put(socketError(error));
  } finally {
    yield call(socketClient.disconnect.bind(socketClient));
  }
}

export function* pollConnect() {
  let pollError;

  for (let i = 0; i < POLL_TIMES; i++) {
    try {
      yield call(socketClient.connect.bind(socketClient));
      return;
    } catch (err) {
      if (i < POLL_TIMES - 1) {
        yield call(delay, POLL_INTERVAL);
      } else {
        pollError = err;
      }
    }
  }

  throw pollError || new Error('Could not connect websocket to server');
}

export function* safePollConnect() {
  try {
    yield call(pollConnect);
  } catch (error) {
    console.log('error detected:', error);
    yield put(socketError(error));
  }
}

export function createEventChannel() {
  return eventChannel(emit => {
    socketClient.on('message', event => {
      try {
        const message = JSON.parse(event.data);
        emit(message);
      // Catch any malformed responses from the server
      } catch (error) {
        emit({ status: RESPONSE_TYPES.SERVER_ERROR, error });
      }
    });
    return () => console.log('Cancelled event channel');
  });
}

export function* socketDispatch(action) {
  const send = (type, contents) => call(socketClient.send.bind(socketClient), type, contents);
  const { type, subtype } = action;
  switch (type) {
    case CREATE_GAME:
      yield send(ACTION_TYPES.CREATE, { name: action.playerId });
      return;
    case CONNECT_TO_GAME:
      yield send(ACTION_TYPES.CONNECT, { game_id: action.gameId, name: action.playerId });
      return;
    case SOCKET_DISPATCH:
      break;
    default:
      throw new Error(`Invalid socket dispatch event type: ${type}`);
  }

  // Handle SOCKET_DISPATCH events
  switch (subtype) {
    case SET_PIECE: {
      const { playerId, move } = action;
      yield send(ACTION_TYPES.MAKE_MOVE, { player_id: playerId, move });
      return;
    }
    default:
      throw new Error(`Invalid socket dispatch subtype: ${subtype}`);
  }
}

export function* watchSocketEvents() {
  const socketChannel = yield call(createEventChannel);
  yield takeEvery(socketChannel, processSocketEvent);
}

export function* processSocketEvent(event) {
  console.log('Got message:', event);
  switch (event.status) {
    case RESPONSE_TYPES.CREATED: {
      yield put(notify.gameCreated(event.payload.gameId));
      break;
    }
    case RESPONSE_TYPES.GAME_CONNECTED: {
      const { gameId, players } = event.payload;
      yield put(notify.gameConnected(gameId, players));
      break;
    }
    case RESPONSE_TYPES.DISCONNECTED: {
      const { player } = event.payload;
      yield put(notify.playerDisconnected(player));
      break;
    }
    case RESPONSE_TYPES.PLAYER_MOVE: {
      // playerId should be either 1 or 2 
      const { player: playerId, move } = event.payload;
      if (playerId != null) {
        yield put(declareMove(playerId, ...move));
      }
      break;
    }
    default:
      break;
  }
}