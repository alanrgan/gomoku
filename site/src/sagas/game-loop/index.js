import { take, select, put, call, race } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import * as chutils from '../../util/chain-utils';
import * as butils from '../../util/board-utils';
import { getTurn, getBoard } from '../../selectors';
import { STATE, toggleTurn, setGameState } from '../../modules/game';
import { DECLARE_MOVE, setPieceAt } from '../../modules/board';
import { isValidMove } from '../../util/board-utils';

import { undoMove, redoMove } from './undo-redo';

export function* startGame(players) {
  while (true) {
    const { run } = yield race({
      run: call(processTurn, players),
      undo: call(undoMove),
      redo: call(redoMove)
    });

    if (run === false) {
      return true;
    }
  }
}

export function* processTurn(players) {
  let gameState;

  const board = yield select(getBoard);

  const chains = yield call(chutils.collect, board.cells);
  gameState = yield call(getGameState, board, chains);

  yield put(setGameState(gameState));

  if (gameState !== STATE.IN_PROGRESS) return false;

  const turn = yield select(getTurn);

  const player = players[turn];
  const move = yield call(promptMove, player);

  yield put(setPieceAt(player.id, ...move));
  yield put(toggleTurn());

  return true;
}

export function* promptMove(player) {
  let move;

  const board = yield select(getBoard);
  if (player.agent) {
    // Need a small delay to allow board to render correctly
    yield call(delay, 300);
    move = yield call(player.makeMove, board.cells, board.squaresRemaining);
  } else {
    while (move == null || !isValidMove(board.cells, ...move)) {
      const action = yield take(DECLARE_MOVE);
      move = action.move;
    }
  }

  return move;
}

export function getGameState(board, chains) {
  // Check for a winner.
  // A winner exists if a player has a chain of length >= 5
  for (let player in chains) {
    player = parseInt(player);
    for (let dir in chains[player]) {
      dir = parseInt(dir);
      for (let i = 5; i < 8; i++) {
        if (chains[player][dir][i].size > 0) {
          return player === butils.PLAYER_ONE ? STATE.PLAYER_ONE_WIN : STATE.PLAYER_TWO_WIN;
        }
      }
    }
  }

  if (board.squaresRemaining <= 0) {
    return STATE.DRAW;
  }

  return STATE.IN_PROGRESS;
}