import { take, put } from 'redux-saga/effects';
import { ActionTypes } from 'redux-undo';
import { toggleTurn } from '../../modules/game';

export function* undoMove() {
  yield take(ActionTypes.UNDO);
  yield put(toggleTurn());
}

export function* redoMove() {
  yield take(ActionTypes.REDO);
  yield put(toggleTurn());
}