import { channel } from 'redux-saga';
import { race, call, take, select, put, fork } from 'redux-saga/effects';
import { startGame } from './game-loop';
import { startGame as start, REMATCH, FORFEIT } from '../modules/game';
import { FINALIZE_FORM } from '../modules/game-form';
import { getPlayers, getBoard } from '../selectors';
import { createBoard } from '../modules/board';
import { setPlayers } from '../modules/players';
import { CREATE_GAME, CONNECT_TO_GAME, notify } from '../modules/socket';
import { START_ONLINE_GAME } from '../modules/online-play';

import { watchSocket } from './socket';
import { ActionCreators } from 'redux-undo';
import AlphaBetaAgent from '../agent/alphabeta';

export default function* metaSaga() {
  const initialGameRace = race({
    local: take(FINALIZE_FORM),
    // Wait for user to request to join a game or create a new one
    // before creating the websocket
    createGame: take(CREATE_GAME),
    connectGame: take(CONNECT_TO_GAME)
  });

  let { local, createGame, connectGame } = yield initialGameRace;
  
  if (local) {
    yield call(initializeGame, local);
  } else {
    const setupChan = yield call(channel);
    yield fork(watchSocket, setupChan, createGame || connectGame);
    yield take(setupChan);
  }

  // Wait for game to have begun
  
  while (true) {
    const players = yield select(getPlayers);
    // Allow players to abandon a game in progress
    const { play, forfeit } = yield race({
      play: call(startGame, players),
      start: call(initializeGame),
      forfeit: take(FORFEIT)
    });

    // If the game has completed normally, wait for a new game to
    // be initialized
    if (play || forfeit) {
      const { restart } = yield race({
        restart: initialGameRace,
        rematch: call(rematch)
      });

      if (restart) {
        if (restart.local) {
          yield call(initializeGame, restart.local);
        }
      }
    }
  }
}

export function* initializeGame(config) {
  if (!config) {
    // Both online and form actions should have the same shape
    let { online, form } = yield race({
      online: take(START_ONLINE_GAME),
      form: take(FINALIZE_FORM)
    });
    config = form || online;
  }

  let { players, aiConfig: { isAi }, board } = config;

  Object.keys(isAi).forEach(playerId => {
    if (isAi[playerId]) {
      const agent =  new AlphaBetaAgent(playerId);
      players[playerId].agent = agent;
      players[playerId].makeMove = agent.makeMove.bind(agent);
    }
  });

  yield put(setPlayers(players));
  yield call(startGameWithBoard, board);
}

export function* rematch() {
  const boardConfig = {...(yield select(getBoard))};
  delete boardConfig.cells;

  yield take(REMATCH);
  yield call(startGameWithBoard, boardConfig);
}

export function* startGameWithBoard(board) {
  yield put(createBoard(board));
  yield put(ActionCreators.clearHistory());
  yield put(start());
  yield put(notify.gameReady());
}

export * from './game-loop';