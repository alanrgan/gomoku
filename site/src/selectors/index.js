export const getPlayers = state => {
  const players = {...state.game.players};
  Object.keys(players).forEach(id => players[id].id = id);
  
  return players;
};
export const getTurn = state => state.game.turn;
export const getBoard = state => state.game.board.present;
export const getBoardCells = state => getBoard(state).cells;
export const getSocketState = state => state.socket;