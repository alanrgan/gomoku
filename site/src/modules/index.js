import { combineReducers } from 'redux';
import gameForm from './game-form';
import game from './game';
import socket from './socket';

const gomokuApp = combineReducers({
  gameForm,
  game,
  socket
});

export default gomokuApp;