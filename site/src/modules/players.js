export const initialState = {
  1: {
    color: 'red'
  },
  2: {
    color: 'blue'
  }
};

export const SET = 'gomoku/players/SET';

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET: {
      return {
        1: {
          color: state[1].color,
          ...action.players[1]
        },
        2: {
          color: state[2].color,
          ...action.players[2]
        }
      }
    }
    default: return state;
  }
}

export function setPlayers(players) {
  return { type: SET, players };
}

export function isInitialized(players) {
  return Object.keys(players)
    .every(playerId => {
      const player = players[playerId];
      return player.agent || (player.name && player.name.trim());
    });
}