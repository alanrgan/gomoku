import players from './players';
import board from './board';
import { newHistory } from 'redux-undo';

export const START_GAME = 'gomoku/game/START_GAME';
export const TOGGLE_TURN = 'gomoku/game/TOGGLE_TURN';
export const REMATCH = 'gomoku/game/REMATCH';
export const FORFEIT = 'gomoku/game/FORFEIT';
export const SET_GAME_STATE = 'gomoku/game/SET_GAME_STATE';

export const STATE = {
  IN_PROGRESS: 1,
  PLAYER_ONE_WIN: 2,
  PLAYER_TWO_WIN: 3,
  DRAW: 4,
  IDLE: 5
};

const initialState = {
  turn: 1,
  gameState: STATE.IDLE,
  board: newHistory([], undefined, [])
};

export default function reducer(state = initialState, action = {}) {
  let { turn, gameState } = state;

  switch (action.type) {
    case START_GAME: {
      gameState = STATE.IN_PROGRESS;
      turn = 1;
      break;
    }
    case TOGGLE_TURN: {
      if (gameState === STATE.IN_PROGRESS) {
        turn = turn === 1 ? 2 : 1;
      }
      break;
    }
    case SET_GAME_STATE: {
      gameState = action.gameState;
      break;
    }
    case FORFEIT: {
      gameState = turn === 1 ? STATE.PLAYER_TWO_WIN : STATE.PLAYER_ONE_WIN
      break;
    }
    default: break;
  }

  return {
    ...state,
    players: players(state.players, action),
    board: board(state.board, action, gameState !== STATE.IN_PROGRESS),
    gameState,
    turn
  };
}

export function startGame() {
  return { type: START_GAME };
}

export function toggleTurn() {
  return { type: TOGGLE_TURN };
}

export function setGameState(state) {
  return { type: SET_GAME_STATE, gameState: state };
}

export function rematch() {
  return { type: REMATCH };
}

export function forfeit() {
  return { type: FORFEIT };
}