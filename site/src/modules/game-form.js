export const SUBMIT_INTENT = 'gomoku/game-form/SUBMIT_INTENT';
export const SET_BOARD = 'gomoku/game-form/SET_BOARD';
export const CLEAR = 'gomoku/game-form/CLEAR';
export const FINALIZE_FORM = 'gomoku/game-form/FINALIZE_FORM';

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case SUBMIT_INTENT:
      return {...state, doSubmit: action.doSubmit};
    case SET_BOARD:
      return {...state, board: action.board};
    case CLEAR:
      return {...state, board: undefined};
    default: return state;
  }
}

export function setGameFormIntent(doSubmit) {
  return { type: SUBMIT_INTENT, doSubmit };
}

export function setBoard(board) {
  return { type: SET_BOARD, board };
}

export function clearForm() {
  return { type: CLEAR };
}

export function finalizeForm(players, board, aiConfig) {
  return { type: FINALIZE_FORM, players, board, aiConfig };
}