import undoable, { includeAction } from 'redux-undo';

const maxDims = {
  rows: 10,
  columns: 10
};

export const initialState = {
  cells: generateBoard(7, 7),
  size: {
    rows: 7,
    columns: 7
  },
  squaresRemaining: 7*7,
  maxDims
};

export const CREATE = 'gomoku/board/CREATE';
export const SET_PIECE = 'gomoku/board/SET_PIECE';
export const DECLARE_MOVE = 'gomoku/board/DECLARE_MOVE';
export const BOARD_STATE = {
  WIN_PLAYER_ONE: 1,
  WIN_PLAYER_TWO: 2,
  DRAW: 3,
  IN_PROGRESS: 0
};

function validateDimensions([columns, rows]) {
  return columns >= 5 && columns <= maxDims.columns && rows >= 5 && rows <= maxDims.rows;
}

export function generateBoard(rows, columns) {
  const board = [];

  for (let i = 0; i < rows; i++) {
    const row = [];
    for (let j = 0; j < columns; j++) {
      row.push(null);
    }
    board.push(row);
  }

  return board;
}

export function reducer(state = initialState, action = {}, isGameOver = false) {
  switch (action.type) {
    case CREATE: {
      const { size: { rows, columns } } = action.board;
      if (!validateDimensions([columns, rows])) {
        return state;
      }

      const squaresRemaining = rows * columns;

      // If board cells are not already provided,
      // generate them from the size property
      if (!action.board.hasOwnProperty('cells')) {
        const cells = generateBoard(rows, columns);
        return {
          ...state,
          ...action.board,
          squaresRemaining,
          cells
        };
      } else {
        return {
          ...state,
          ...action.board,
          squaresRemaining
        }
      }
    }
    case SET_PIECE: {
      if (isGameOver) return state;

      const { row, col, piece } = action;
      if (row < 0 || row >= state.size.rows ||
          col < 0 || col >= state.size.columns) {
        return state;
      }

      const cells = state.cells.map(r => r.slice());
      cells[row][col] = parseInt(piece);

      // Decrement remaining squares available
      let { squaresRemaining } = state;
      squaresRemaining -= 1;

      return {
        ...state,
        cells,
        squaresRemaining
      };
    }
    default: return state;
  }
}

export default undoable(reducer, {
  filter: includeAction([CREATE, SET_PIECE])
})

export function createBoard(board) {
  return { type: CREATE, board };
}

export function setPieceAt(piece, row, col) {
  return { type: SET_PIECE, row, col, piece };
}

export function declareMove(piece, row, col) {
  return { type: DECLARE_MOVE, move: [row, col], piece };
}