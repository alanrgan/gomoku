import { START_ONLINE_GAME } from './online-play';

/*
*  Base socket action type. All other socket dispatch actions will have a
*  subtype, which functions similarly to ordinary actions.
*  The base type is used to buffer socket dispatch actions before a socket connection
*  has been established.
*/
export const SOCKET_DISPATCH = 'gomoku/socket/SOCKET_DISPATCH';

export const CONNECT = 'gomoku/socket/CONNECT';
export const CONNECT_SUCCESS = 'gomoku/socket/CONNECT_SUCCESS';
export const DISCONNECT = 'gomoku/socket/DISCONNECT';
export const DISCONNECT_SUCCESS = 'gomoku/socket/DISCONNECT_SUCCESS';
export const RECONNECT = 'gomoku/socket/RECONNECT';
export const SOCKET_ERROR = 'gomoku/socket/SOCKET_ERROR';

/*
* Socket action subtypes
*/

export const CONNECT_TO_GAME = 'gomoku/socket/subtype/CONNECT_TO_GAME';
export const CREATE_GAME = 'gomoku/socket/subtype/CREATE_GAME';

export const NOTIFY = {
  GAME: {
    CREATED: 'gomoku/socket/NOTIFY/GAME/CREATED',
    CONNECTED: 'gomoku/socket/NOTIFY/GAME/CONNECTED',
    READY: 'gomoku/socket/NOTIFY/GAME/READY'
  },
  PLAYER: {
    DISCONNECTED: 'gomoku/socket/NOTIFY/PLAYER/DISCONNECTED'
  }
};

const initialState = {
  error: null,
  connected: false,
  gameId: null,
  // Name of the player on the local machine
  localPlayerName: null,
  players: null,
  gameStarted: false,
  gameReady: false
};

export default function reducer(state = initialState, action = {}) {
  switch(action.type) {
    case CONNECT_SUCCESS: {
      return {
        ...state,
        connected: true
      };
    }
    case DISCONNECT_SUCCESS: {
      return {
        ...state,
        connected: false
      };
    }
    case CONNECT_TO_GAME:
    case CREATE_GAME: {
      return {
        ...state,
        localPlayerName: action.playerId
      };
    }
    case START_ONLINE_GAME: {
      return {
        ...state,
        gameStarted: true
      };
    }
    case NOTIFY.GAME.READY: {
      return {
        ...state,
        gameReady: true
      };
    }
    case NOTIFY.GAME.CREATED: {
      return {
        ...state,
        gameId: action.gameId,
      };
    }
    case NOTIFY.GAME.CONNECTED: {
      return {
        ...state,
        players: action.players,
        gameId: action.gameId,
      };
    }
    case NOTIFY.PLAYER.DISCONNECTED: {
      let players = [...(state.players || [])];
      players = players.filter(player => player !== action.player);
      return {
        ...state,
        players
      };
    }
    default: return state;
  }
}

export function socketDispatch(subtype, payload = {}) {
  return { type: SOCKET_DISPATCH, subtype, ...payload };
}

export function connect() {
  return { type: CONNECT };
}

export function socketError(error) {
  return { type: SOCKET_ERROR, error };
}

export function disconnect() {
  return { type: DISCONNECT };
}

export function reconnect() {
  return { type: RECONNECT };
}

export function connectToGame(playerId, gameId) {
  return { type: CONNECT_TO_GAME, playerId, gameId };
}

export function createGame(playerId) {
  return { type: CREATE_GAME, playerId };
}

export const notify = {
  gameCreated: gameId => ({ type: NOTIFY.GAME.CREATED, gameId }),
  gameConnected: (gameId, players) => ({ type: NOTIFY.GAME.CONNECTED, gameId, players }),
  gameReady: () => ({ type: NOTIFY.GAME.READY }),
  playerDisconnected: player => ({ type: NOTIFY.PLAYER.DISCONNECTED, player }),
};