import { SET_PIECE } from './board';
import { socketDispatch } from './socket';
import { initialState as initialPlayers } from './players';
import { initialState as initialBoard } from './board';
import clone from 'lodash/cloneDeep';

export const START_ONLINE_GAME = 'gomoku/online-player/START_ONLINE_GAME';

export const sendMove = (playerId, move) => {
  return socketDispatch(SET_PIECE, { playerId, move });
};

export const startOnlineGame = playerNames => {
  const players = clone(initialPlayers);
  const board = clone(initialBoard);
  const aiConfig = { isAi: { 1: false, 2: false } };

  playerNames.forEach((player, idx) => {
    players[idx+1].name = player;
  });

  return { type: START_ONLINE_GAME, players, board, aiConfig };
};