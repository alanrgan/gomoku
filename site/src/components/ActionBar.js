import React from 'react';
import { Flex } from '@rebass/grid';
import { Card } from '@material-ui/core';

import UndoRedo from '../containers/UndoRedo';
import ForfeitRematch from '../containers/ForfeitRematch';

export default class ActionBar extends React.Component {
  render() {
    return (
      <Flex
        as={Card}
        style={{borderRadius: '0px', flexDirection: 'row'}}>
        <UndoRedo {...this.props.undoRedoProps} />
        <ForfeitRematch />
      </Flex>
    );
  }
}