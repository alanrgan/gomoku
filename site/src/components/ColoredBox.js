import styled from 'styled-components';

export default styled.div`
  background-color: ${props => props.color || '#000'};
  ${props => props.noShadow ? '' : 'box-shadow: #ddd 1px 1px 3px'};
  border: solid #ccc 1px;
  width: ${20/16}rem;
  height: ${20/16}rem;

  :focus {
    outline: 0px;
  }
`;