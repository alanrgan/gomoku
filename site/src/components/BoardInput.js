import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';
import styled from 'styled-components';

const RemoveButton = styled(Button)`
  background-color: #bc0303${props => (props.color === 'error') ? '!important' : ''};
  color: #fff!important;
`;

class BoardInput extends React.Component {
  state = {
    imported: false,
    label: 'Import Board'
  };

  handleRemoveLabelEnter = () => this.setState({ label: 'Remove' });
  handleRemoveLabelLeave = () => this.setState({ label: 'Imported' });

  handleBoardInput = event => {
    if (!event.target || (event.target.files && event.target.files.length === 0)) return;
    const reader = new FileReader();
    reader.addEventListener('load', evt => {
      const board = JSON.parse(evt.target.result);
      this.setState({ imported: true, label: 'Imported' });
      this.props.onBoardLoaded(board);
    });
    reader.readAsText(event.target.files[0]);
  };

  removeImportedBoard = () => {
    this.fileInput.value = '';
    this.setState({ imported: false, label: 'Import Board' });
    this.props.onBoardRemove();
  };

  render() {
    const importButton = (
      <label htmlFor={this.props.id}>
        <Button
          component='span'
          size='small'
          variant='contained'
          color={this.state.label === 'Imported' ? 'secondary' : 'primary'}
        >
          {this.state.label}
        </Button>
      </label>
    );

    const removeBoardButton = (
      <RemoveButton
        variant='contained'
        color={(this.state.label === 'Remove') ? 'error' : 'secondary'}
        size='small'
        onMouseEnter={this.handleRemoveLabelEnter}
        onMouseLeave={this.handleRemoveLabelLeave}
        onClick={this.removeImportedBoard}
      >
        {this.state.label}
      </RemoveButton>
    );

    return (
      <div>
        <input
          ref={el => this.fileInput = el}
          id={this.props.id}
          style={{display: 'none'}}
          type='file'
          accept='.json'
          onChange={this.handleBoardInput}/>
        {this.state.imported ? removeBoardButton : importButton}
      </div>
    );
  }
}

BoardInput.propTypes = {
  id: PropTypes.string.isRequired,
  onBoardLoaded: PropTypes.func.isRequired,
  onBoardRemove: PropTypes.func.isRequired
};

export default BoardInput;