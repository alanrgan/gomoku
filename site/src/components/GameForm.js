import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import {
  TextField,
  FormControlLabel,
  Switch,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Input
} from '@material-ui/core';
import { Flex, Box } from '@rebass/grid';

import ColorPicker from './ColorPicker';
import DimensionSelector from './DimensionSelector';

const PlayerBox = styled(Box)`
  width: 167px;
  height: 48px;
`;

export class GameForm extends React.Component {
  static propTypes = {
    notifyFormIntent: PropTypes.func.isRequired,
    onFormCompleted: PropTypes.func.isRequired,
    importedBoard: PropTypes.object,
    onMount: PropTypes.func,
    doSubmit: PropTypes.bool.isRequired
  };

  static defaultProps = {
    onMount: () => {}
  };

  state = {
    players: {
      1: {
        color: 'red'
      },
      2: {
        color: 'blue'
      }
    },
    board: {
      size: {
        rows: 7,
        columns: 7
      }
    },
    aiConfig: {
      isAi: {
        1: false,
        2: false
      },
      types: [1,1]
    }
  };

  onFormSubmit = () => {
    this.props.notifyFormIntent(false);

    const finalState = {...this.state};
    if (this.props.importedBoard) {
      finalState.board = this.props.importedBoard;
    }
    this.props.onFormCompleted(finalState);
    this.props.history.push('/game/');
  };

  componentDidUpdate() {
    if (this.props.doSubmit) {
      if (this.validateInputs()) {
        this.onFormSubmit();
      } else {
        this.props.notifyFormIntent(false);
      }
    }
  }

  componentDidMount() {
    this.props.onMount();
  }

  validateInputs = () => {
    return Object.keys(this.state.players)
      .every(playerId => {
        const player = this.state.players[playerId];
        const isAi = this.state.aiConfig.isAi[playerId];
        return isAi || (player.name && player.name.trim());
      });
  };

  reducePlayerProp = (idx, propName, value) => {
    this.setState(state => {
      const player = state.players[idx];
      return ({
        players: {
          ...state.players,
          [idx]: {
            ...player,
            [propName]: value
          } 
        }
      });
    });
  };

  onPlayerInputChange = idx => event => this.reducePlayerProp(idx, 'name', event.target.value);
  onPlayerColorChange = idx => color => this.reducePlayerProp(idx, 'color', color.hex);

  handleHeightChange = event => {
    const height = event.target.value;
    this.setState(state => ({
      board: {
        ...state.board,
        size: {
          ...state.board.size,
          rows: height
        }
      }
    }));
  };

  handleWidthChange = event => {
    const width = event.target.value;
    this.setState(state => ({
      board: {
        ...state.board,
        size: {
          ...state.board.size,
          columns: width
        }
      }
    }));
  }

  onAiSelectChange = idx => event => {
    const isAi = {...this.state.aiConfig.isAi, [idx]: event.target.checked };
    this.setState(state => ({
      aiConfig: {
        ...state.aiConfig,
        isAi
      }
    }));
  };

  render() {
    return (
      <Flex
        flexDirection='column'
        as='form'
        onSubmit={this.onFormSubmit}>
        <Flex flexDirection='row' justifyContent='space-between'>
          <Flex flexDirection='column'
                alignItems='center'>
            {
              ['One', 'Two'].map((num, idx) => (
                <PlayerBox
                  mt={2}
                  key={idx}
                >
                  {
                    !this.state.aiConfig.isAi[idx+1] && (
                      <TextField
                        label={`Player ${num}`}
                        placeholder='Name'
                        value={this.state.players[idx+1].name || ''}
                        onChange={this.onPlayerInputChange(idx+1)}
                        InputLabelProps={{shrink: true}}
                        required
                      />
                    )
                  }
                  {
                    this.state.aiConfig.isAi[idx+1] && (
                      <FormControl
                        style={{display: 'flex'}}
                        required
                      >
                        <InputLabel htmlFor={`ai${num}`}>{`AI ${num}`}</InputLabel>
                        <Select
                          value={this.state.aiConfig.types[idx]}
                          input={<Input id={`ai${num}`} name='ai' />}
                        >
                          <MenuItem value={1}>Hard</MenuItem>
                        </Select>
                      </FormControl>
                    )
                  }
                </PlayerBox>
              ))
            }
          </Flex>
          <Flex flexDirection='column' alignItems='center'>
            {
              [0,1].map(idx => (
                <Box mt={24 + idx*8} ml={3} mr={3} key={idx}>
                  <ColorPicker
                    color={this.state.players[idx+1].color}
                    onColorChange={this.onPlayerColorChange(idx + 1)}
                  />
                </Box>
              ))
            }
          </Flex>
          <Flex flexDirection='column' alignItems='center'>
            <Box mt={2}>
              <FormControlLabel
                control={
                  <Switch
                    checked={this.state.aiConfig.isAi[1]}
                    onChange={this.onAiSelectChange(1)}
                    value={'isAi1'}
                  />
                }
                label='AI'
              />
            </Box>
            <Box mt={2}>
              <FormControlLabel
                control={
                  <Switch
                    checked={this.state.aiConfig.isAi[2]}
                    onChange={this.onAiSelectChange(2)}
                    value={'isAi2'}
                  />
                }
                label='AI'
              />
            </Box>
          </Flex>
        </Flex>
        <Flex>
          <Box mt={3}>
            <DimensionSelector
              type='width'
              label='Width'
              defaultValue={this.state.board.size.columns}
              id='gomoku-board-width'
              onChange={this.handleWidthChange}
            />
          </Box>
          <Box mt={3} ml={3}>
            <DimensionSelector
              type='height'
              label='Height'
              value={this.state.board.size.rows}
              id='gomoku-board-height'
              onChange={this.handleHeightChange}
            />
          </Box>
        </Flex>
      </Flex>
    );
  }
};

export default withRouter(GameForm);