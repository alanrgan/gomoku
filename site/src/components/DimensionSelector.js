import React from 'react';
import {
  FormControl,
  Select,
  InputLabel
} from '@material-ui/core';

const generateRange = (start, end) => {
  const range = [];
  for (let n = start; n <= end; n++) {
    range.push(n);
  }
  return range;
};

const DimensionSelector = props => {
  const values = props.values || generateRange(5, 10);
  return (
    <FormControl>
      <InputLabel htmlFor={props.id}>{props.label}</InputLabel>
      <Select
        native
        {...props}
      >
        {
          values.map((val, i) => (
            <option value={val} key={i}>{val}</option>
          ))
        }
      </Select>
      
    </FormControl>
  );
};

export default DimensionSelector;