import React from 'react';
import { ChromePicker } from 'react-color';
import Tippy from '@tippy.js/react';

import 'tippy.js/dist/tippy.css';
import ColoredBox from './ColoredBox';


class ColorPicker extends React.Component {
  render() {
    const picker = (
      <ChromePicker
        color={this.props.color}
        onChange={this.props.onColorChange}
      />
    );

    return (
      <Tippy content={picker} trigger='click' interactive>
        <ColoredBox color={this.props.color} />
      </Tippy>
    );
  }
};

export default ColorPicker;