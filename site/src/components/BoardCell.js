import React from 'react';
import { Flex } from '@rebass/grid';
import styled, { css } from 'styled-components';

const sizes = {
  cell: 34,
  piece: 26
};

const media = {
  phone: Object.keys(sizes).reduce((acc, label) => {
    acc[label] = () => css`
      @media (max-width: ${400/16}em), (max-height: ${445/16}em) {
        ${css`
          width: ${sizes[label]/16}rem;
          height: ${sizes[label]/16}rem;
        `}
      }
    `;

    return acc;
  },  {})
};

const FixedFlex = styled(Flex)`
  width: ${50/16}rem;
  height: ${50/16}rem;
  border: solid #ddd 1px;

  ${media.phone.cell()}
`;

const Piece = styled.div`
  border-radius: 50%;
  width: ${40/16}rem;
  height: ${40/16}rem;
  background-color: ${props => props.color}
  box-shadow: #aaa 1px 1px 3px;

  ${media.phone.piece()}
`;

const BoardCell = ({ color, onClick }) => {
  return (
    <FixedFlex
      justifyContent='center'
      alignItems='center'
      onClick={onClick}>
      {color && <Piece color={color} />}
    </FixedFlex>
  );
};

export default BoardCell;