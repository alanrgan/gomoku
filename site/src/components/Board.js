import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Card } from '@material-ui/core';
import { Flex } from '@rebass/grid';
import HotKeys from 'react-hot-keys';
import saveAs from 'file-saver';
import isEqual from 'lodash/isEqual';

import CenterFlex from './CenterFlex';
import BoardCell from './BoardCell';
import StatusBar from './StatusBar';
import ActionBar from './ActionBar';
import { isInitialized } from '../modules/players';

export class Board extends React.Component {
  static propTypes = {
    onCellClick: PropTypes.func.isRequired,
    players: PropTypes.object.isRequired,
    board: PropTypes.shape({
      cells: PropTypes.array.isRequired,
      size: PropTypes.shape({
        rows: PropTypes.number.isRequired,
        columns: PropTypes.number.isRequired
      }),
      squaresRemaining: PropTypes.number.isRequired,
      maxDims: PropTypes.shape({
        rows: PropTypes.number.isRequired,
        columns: PropTypes.number.isRequired
      })
    }).isRequired,
    isOnline: PropTypes.bool.isRequired,
    ownPieceId: PropTypes.number
  };

  componentDidMount() {
    if (!isInitialized(this.props.players)) {
      this.props.history.push('/');
    }
  }

  shouldComponentUpdate(nextProps) {
    return !isEqual(nextProps, this.props);
  }

  generateCellClickCallback = (rowN, colN) => {
    if (!this.props.isOnline || this.props.turn === this.props.ownPieceId) {
      return this.props.onCellClick(this.props.turn, rowN, colN, {...this.props});
    }
    return undefined;
  };

  saveBoard = () => {
    const serializedBoard = JSON.stringify(this.props.board);
    const blob = new Blob([serializedBoard], { type: 'text/json;charset=utf-8'});
    saveAs(blob, 'gomokuBoard.json');
  };

  renderBoard = () => {
    const { rows, columns } = this.props.board.size;
    const board = this.props.board.cells.map((row, rowN) => (
      <Flex key={'' + rowN}>
        {
          row.map((pid, colN) => {
            const player = this.props.players[pid];

            return (
              <BoardCell
                key={`${rowN}${colN}`}
                color={pid && player.color}
                onClick={!pid ? this.generateCellClickCallback(rowN, colN) : undefined}
                width={1/columns}
                id={rowN*rows + colN} />
            );
          })
        }
      </Flex>
    ));

    return board;
  };

  render() {
    return (
      <HotKeys
        keyName='ctrl+shift+s'
        onKeyDown={this.saveBoard}
      >
        <StatusBar
          players={this.props.players}
          turn={this.props.turn}
          gameState={this.props.gameState}
        />
        <CenterFlex flexStyle={{minHeight: 90}}>
          <ActionBar
            undoRedoProps={{disabled: this.props.isOnline}}/>
          <Card>
            {this.props.board && this.renderBoard()}
          </Card>
        </CenterFlex>
      </HotKeys>
    );
  }
}

export default withRouter(Board);