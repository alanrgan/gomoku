import React from 'react';
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import { ArrowLeft, ArrowRight } from '@material-ui/icons';
import { Box } from '@rebass/grid';
import styled from 'styled-components';
import ColoredBox from './ColoredBox';

import { PLAYER_ONE, PLAYER_TWO } from '../util/board-utils';
import { STATE } from '../modules/game';

const Grow = styled.div`
  flex-grow: 1;
`;

const StyledToolbar = styled(Toolbar)`
  background: #d85928;
`;

class StatusBar extends React.Component {
  generateStatusLabel = () => {
    switch (this.props.gameState) {
      case STATE.PLAYER_ONE_WIN:
        return `${this.props.players[1].name} wins!`;
      case STATE.PLAYER_TWO_WIN:
        return `${this.props.players[2].name} wins!`;
      case STATE.DRAW:
        return "It's a draw";
      default:
        return '';
    }
  };

  render() {
    return (
      <AppBar position='static'>
        <StyledToolbar variant='dense'>
          <Typography variant='h6' color='inherit'>
            {this.props.players[1].name}
          </Typography>
          <Box ml={2} mr={2}>
            <ColoredBox noShadow color={this.props.players[1].color} />
          </Box>
          {this.props.turn === PLAYER_ONE && <ArrowLeft />}
          <Grow />
          {this.props.gameState !== STATE.IN_PROGRESS && (
            <Typography variant='subheading' color='inherit'>
              {this.generateStatusLabel()}
            </Typography>
          )}
          <Grow />
          {this.props.turn === PLAYER_TWO && <ArrowRight />}
          <Box ml={2} mr={2}>
            <ColoredBox noShadow color={this.props.players[2].color} />
          </Box>
          <Typography variant='h6' color='inherit'>
            {this.props.players[2].name}
          </Typography>
        </StyledToolbar>
      </AppBar>
    );
  }
}

export default StatusBar;