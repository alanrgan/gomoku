import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from '@rebass/grid';
import {
  Card,
  CardHeader,
  Tab,
  Tabs,
  Button,
  CardContent,
  CardActions,
  Typography
} from '@material-ui/core';

import CenterFlex from './CenterFlex';
import GomokuForm from '../containers/GomokuForm';
import BoardInput from './BoardInput';
import OnlineForm from '../containers/OnlineForm';

const StyledMenuCard = styled(Card)`
  min-width: ${275/16}rem;
`;

const CardTitle = styled(Typography)`
  font-size: 24;
`;

class MenuCard extends React.Component {
  state = {
    activeTab: this.props.match.params.gameId ? 1 : 0,
    formStep: 0
  };

  switchTabs = (event, activeTab) => {
    this.setState({ activeTab });
  };

  makeFormStep = step => {
    this.setState(state => ({ formStep: step || (state.formStep + 1) }));
  };

  render() {
    const { activeTab } = this.state;
    const { gameId } = this.props.match.params;
    
    return (
      <CenterFlex>
        <Box px={2}>
          <StyledMenuCard>
            <CardHeader
              style={{padding: 0}}
              subheader={
                <Tabs
                  style={{maxWidth: '375px'}}
                  fullWidth
                  value={this.state.activeTab}
                  indicatorColor='primary'
                  textColor='primary'
                  onChange={this.switchTabs}>
                  <Tab label='Main' />
                  <Tab label='Online' />
                </Tabs>
              }
            />
            <CardContent>
              <CardTitle color='textSecondary' gutterBottom>
                Gomoku
              </CardTitle>
              {(activeTab === 0) && <GomokuForm/>}
              {
                (activeTab === 1) && (
                  <OnlineForm
                    gameId={gameId}
                    activeStep={this.state.formStep}
                    onStep={this.makeFormStep} />
                )
              }
            </CardContent>
            {
              (activeTab === 0 || (activeTab === 1 && this.state.stepperDone)) && (
                <CardActions>
                  <Button
                    onClick={this.props.onFormSubmit}
                    size='small'
                    variant='contained'
                    color='primary'>
                    Play
                  </Button>
                  <BoardInput
                    id='board-input'
                    onBoardLoaded={this.props.onBoardLoaded}
                    onBoardRemove={this.props.onBoardRemove}
                  />
                </CardActions>
              )
            }
          </StyledMenuCard>
        </Box>
      </CenterFlex>
    );
  }
};

MenuCard.propTypes = {
  onFormSubmit: PropTypes.func.isRequired
};

export default MenuCard;