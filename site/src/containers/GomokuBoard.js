import { connect } from 'react-redux';
import Board from '../components/Board';
import { declareMove } from '../modules/board';
import { STATE } from '../modules/game';
import { sendMove } from '../modules/online-play';

const mapStateToProps = ({ game, socket }) => {
  const { board, players, turn, gameState } = game;
  const { gameStarted: isOnline, localPlayerName } = socket;

  let ownPieceId = null;

  Object.keys(players).forEach(id => {
    if (players[id].name === localPlayerName) {
      ownPieceId = parseInt(id);
    }
  });

  return {
    board: board.present,
    players,
    turn,
    gameState,
    isOnline,
    // Piece id (1 or 2) of the local player, if there is one
    ownPieceId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCellClick: (piece, row, col, ownProps = {}) => () => {
      const { isOnline, gameState } = ownProps;
      if (!isOnline) {
        dispatch(declareMove(piece, row, col));
      // Online play
      } else if (gameState === STATE.IN_PROGRESS) {
        dispatch(sendMove(piece, [row, col]));
      }
    },
  };
};

const GomokuBoard = connect(
  mapStateToProps,
  mapDispatchToProps
)(Board);

export default GomokuBoard;