import { connect } from 'react-redux';
import MenuCard from '../components/MenuCard';
import { setGameFormIntent, setBoard, clearForm } from '../modules/game-form';

const mapDispatchToProps = dispatch => {
  return {
    onFormSubmit: () => dispatch(setGameFormIntent(true)),
    onBoardLoaded: board => dispatch(setBoard(board)),
    onBoardRemove: () => dispatch(clearForm())
  };
};

const Menu = connect(
  undefined,
  mapDispatchToProps
)(MenuCard);

export default Menu;