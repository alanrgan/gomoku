import React from 'react';
import { connect } from 'react-redux';
import { IconButton } from '@material-ui/core';
import FlagRounded from '@material-ui/icons/FlagRounded';
import Replay from '@material-ui/icons/Replay';

import { STATE, rematch, forfeit } from '../modules/game';

export class ForfeitRematch extends React.Component {
  render() {
    return (
      <IconButton style={{padding: '9px'}}>
        { this.props.gameInProgress && <FlagRounded onClick={this.props.onForfeit} /> }
        { !this.props.gameInProgress && <Replay onClick={this.props.onRematch} /> }
      </IconButton>
    )
  }
}

const mapStateToProps = state => {
  return {
    gameInProgress: state.game.gameState === STATE.IN_PROGRESS
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onForfeit: () => dispatch(forfeit()),
    onRematch: () => dispatch(rematch())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ForfeitRematch);