import React from 'react';
import { IconButton } from '@material-ui/core';
import { Undo, Redo } from '@material-ui/icons';
import { connect } from 'react-redux';
import { ActionCreators } from 'redux-undo';
import { STATE } from '../modules/game';

const UndoRedo = ({ onUndo, canUndo, onRedo, canRedo, disabled }) => (
  [
    <IconButton style={{padding: '9px'}} disabled={disabled || !canUndo} key={1}>
      <Undo onClick={onUndo} />
    </IconButton>,
    <IconButton style={{padding: '9px'}} disabled={disabled || !canRedo} key={2}>
      <Redo onClick={onRedo} />
    </IconButton>
  ]
);

const mapStateToProps = state => {
  const isInProgress = state.game.gameState === STATE.IN_PROGRESS;
  
  return {
    canUndo: state.game.board.past.length > 0 && isInProgress,
    canRedo: state.game.board.future.length > 0 && isInProgress
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onUndo: () => dispatch(ActionCreators.undo()),
    onRedo: () => dispatch(ActionCreators.redo())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UndoRedo);