import { connect } from 'react-redux';
import GameForm from '../components/GameForm';
import { setGameFormIntent, clearForm, finalizeForm } from '../modules/game-form';

const mapStateToProps = ({ gameForm: { doSubmit, board }, game }) => {
  const { players } = game;

  return {
    doSubmit,
    players,
    importedBoard: board
  };
};

const mapDispatchToProps = dispatch => {
  return {
    notifyFormIntent: didSucceed => dispatch(setGameFormIntent(didSucceed)),
    onFormCompleted: ({ players, board, aiConfig }) => dispatch(finalizeForm(players, board, aiConfig)),
    onMount: () => dispatch(clearForm())
  };
};

const GomokuForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(GameForm);

export default GomokuForm;