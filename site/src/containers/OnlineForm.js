import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Flex, Box } from '@rebass/grid';

import {
  TextField,
  Button
} from '@material-ui/core';

import {
  connectToGame,
  createGame
} from '../modules/socket';

import { startOnlineGame } from '../modules/online-play';
import { STATE } from '../modules/game';

export class OnlineForm extends React.Component {
  state = {
    gameId: '',
    playerId: '',
  };

  handleConnect = () => {
    if (this.state.gameId !== '') {
      this.props.onStep();
      this.props.connectToGame(this.state.playerId, this.state.gameId);
    }
  };

  handleCreateGame = () => {
    this.props.onStep();
    this.props.onCreateGame(this.state.playerId);
  };

  handleGameIdChange = event => this.setState({ gameId: event.target.value.trim() });
  handlePlayerIdChange = event => this.setState({ playerId: event.target.value.trim() });

  handleNext = () => {
    if (this.state.playerId === '') {
      return;
    }
    
    if (this.props.gameId) {
      this.props.onStep(2);
      this.props.connectToGame(this.state.playerId, this.props.gameId);
    } else {
      this.props.onStep();
    }
  };

  render() {
    if (this.props.gameReady) {
      return <Redirect to='/game' />;
    } else if (this.props.players && this.props.players.length === 2) {
      this.props.onGameReady(this.props.players);
    }

    return (
      <Flex flexDirection='column'>
        {
          this.props.activeStep === 0 && [
            <Flex key={1}>
              <TextField
                label='Player Name'
                placeholder='Name'
                value={this.state.playerId}
                onChange={this.handlePlayerIdChange}
                InputLabelProps={{shrink: true}}
              />
            </Flex>,
            <Box
              key={2}
              pt={3}
              alignSelf='flex-end'
            >
              <Button
                variant='contained'
                color='primary'
                size='small'
                onClick={this.handleNext}
              >
                Next
              </Button>
            </Box>
          ]
        }
        {
          this.props.activeStep === 1 && ([
            <Flex flexDirection='row' key={1}>
              <TextField
                label='Game ID'
                placeholder='ID'
                value={this.state.gameId}
                onChange={this.handleGameIdChange}
                InputLabelProps={{shrink: true}}
              />
              <Box pt={3} pl={4}>
                <Button
                  variant='contained'
                  color='primary'
                  size='small'
                  onClick={this.handleConnect}
                >
                  Connect
                </Button>
              </Box>
            </Flex>,
            <Box alignSelf='center' py={3} key={2}>or</Box>,
            <Box alignSelf='center' key={3}>
              <Button
                variant='contained'
                color='primary'
                size='small'
                onClick={this.handleCreateGame}
              >
                Create Game
              </Button>
            </Box>
          ])
        }
        {
          this.props.activeStep === 2 && (
            <Flex flexDirection='column'>
              <Box>
                {this.props.gameId && `Game ID: ${this.props.gameId}`}
                {!this.props.gameId && 'Waiting for server...'}
              </Box>
              <Box>
                { this.props.players && ('Players are: ' + this.props.players.join(', ')) }
              </Box>
            </Flex>
          )
        }
      </Flex>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    gameId: ownProps.gameId || state.socket.gameId,
    players: state.socket.players,
    gameReady: state.socket.gameReady
  };
};

const mapDispatchToProps = dispatch => {
  return {
    connectToGame: (playerId, gameId) => dispatch(connectToGame(playerId, gameId)),
    onCreateGame: playerId => dispatch(createGame(playerId)),
    onGameReady: players => dispatch(startOnlineGame(players))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OnlineForm);