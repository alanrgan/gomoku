export default class Move {
  static wrap(move) {
    return new Move(move);
  }

  constructor(move) {
    this.move = JSON.stringify(move);
  }

  equals(other) {
    if (other instanceof Move) {
      return this.move === other.move;
    } else if (typeof other === 'string') {
      return this.move === JSON.stringify(other);
    } else {
      return false;
    }
  }

  unwrap() {
    return JSON.parse(this.move);
  }
};