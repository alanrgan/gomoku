export default class SocketClient {
  socket;

  constructor(host) {
    this.host = host;
    this.eventHandlers = {};
  }

  connect() {
    if (this.socket) {
      return Promise.reject('Invalid connect request: already connected to the host. Disconnect before connecting again.');
    } else {
      const client = this;
      return new Promise((resolve, reject) => {
        client.socket = new WebSocket(client.host);
        client.socket.onopen = resolve;
        client.socket.onclose = err => {
          client.socket = null;
          reject(err);
        };
      });
    }
  }

  send(tag, contents) {
    if (this.socket) {
      const message = JSON.stringify({ tag, body: JSON.stringify(contents) });
      console.log('sending message', message);
      this.socket.send(message);
    } else {
      throw new Error('Invalid state error: socket is disconnected');
    }
  }

  disconnect() {
    if (this.socket) {
      this.socket.close();
    }
    this.socket = null;
  }

  on(event, callback) {
    if (!this.socket) {
      throw new Error('Invalid state error: socket is disconnected');
    }
    this.socket[`on${event}`] = callback;
    this.eventHandlers[`on${event}`] = callback;
  }
}

/*
* Socket response types
*/
export const RESPONSE_TYPES = {
  SERVER_ERROR: 'SERVER_ERROR',
  CREATED: 'CREATED',
  GAME_CONNECTED: 'GAME_CONNECTED',
  DISCONNECTED: 'DISCONNECTED',
  PLAYER_MOVE: 'PLAYER_MOVE'
};

export const ACTION_TYPES = {
  CONNECT: 'connect',
  CREATE: 'create',
  DISCONNECT: 'disconnect',
  MAKE_MOVE: 'make_move',
};