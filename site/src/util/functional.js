
export function* filter(fn, generator) {
  for (let x of generator) {
    if (fn(x)) {
      yield x;
    }
  }
}

export function* map(fn, generator) {
  for (let x of generator) {
    yield fn(x);
  }
}

export function reduce(fn, generator, initialValue) {
  let acc = initialValue;
  for (let x of generator) {
    acc = fn(x, acc);
  }
  return acc;
}