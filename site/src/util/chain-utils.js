import * as butils from './board-utils';
import { directionToCmp } from './board-utils';
import * as arrays from './arrays';

// Chain end markers. Distinguishes between the two ends of a chain
export const HEAD = 'HEAD';
export const TAIL = 'TAIL';

/**
 * A chain is defined to be a contiguous (unbroken) sequence of a single player's
 * pieces on a Gomoku board. A chain can exist along any direction.
 */

export function makeDefaultChainSet() {
  const chains = {};
  [butils.PLAYER_ONE, butils.PLAYER_TWO].forEach(player => {
    chains[player] = {};

    butils.DIRECTIONS.forEach(dir => {
      chains[player][dir] = arrays.fill(() => new Set(), 8);
    });
  });

  return chains;
}

/**
 * Collect all chains from a 2-d array of board cells.
 * 
 * Returns an object C, where C[playerNum][direction][n] yields an array
 * of chains of length n along 'direction' for the specified player.
 * 
 * @param {Array} cells 
 */
export function collect(cells) {
  const chains = makeDefaultChainSet();

  butils.DIRECTIONS.forEach(dir => {
    const iter = butils.iterate({ cells, direction: dir });
    let currentChain = [];

    let prevElem = butils.EMPTY;
    
    for (let elem of iter) {
      /*
        Conditions for appending the current chain:
          1. If we reach a block end: chains do not continue past row / column / diagonal boundaries
          2. If we reach an empty square: By definition, any existing chain ends on an empty square or a boundary
          3. If we reach a piece that is different from the previous one: a chain is a contiguous seq of the same piece type
      */

      const shouldAppendChain = elem === butils.BLOCK_END || elem.value === butils.EMPTY || elem.value !== prevElem;
      if (shouldAppendChain) {
        const n = currentChain.length;
        // We are only concerned about chains of length > 1.
        // This takes care of 'chains' of empty squares
        if (n > 1) {
          // prevElem := player who the chain belongs to
          chains[prevElem][dir][n].add(JSON.stringify(currentChain));
        }
        currentChain = [];
      }

      if (elem !== butils.BLOCK_END) {
        currentChain.push(elem.coord);
        prevElem = elem.value;
      }

    }
  });

  return chains;
}

export function getChainEnds({ chain, direction, withEndMarkers = false }) {
  const sortedChain = chain.concat().sort(directionToCmp[direction]);
  const head = sortedChain[0];
  const tail = sortedChain[sortedChain.length - 1];

  let directionOffsets;
  switch (direction) {
    case butils.HORIZONTAL:
      directionOffsets = [[0, -1], [0, 1]];
      break;
    case butils.VERTICAL:
      directionOffsets = [[-1, 0], [1, 0]];
      break;
    case butils.DIAGONAL:
      directionOffsets = [[-1, 1], [1, -1]];
      break;
    case butils.ANTI_DIAGONAL:
      directionOffsets = [[-1, -1], [1, 1]];
      break;
    default:
      return [];
  };

  const ends = [];
  [[head, HEAD], [tail, TAIL]].forEach(([endCoord, type], i) => {
    const [dx, dy] = directionOffsets[i];
    const end = [endCoord[0] + dx, endCoord[1] + dy];
    if (!withEndMarkers) {
      ends.push(end);
    } else {
      ends.push({ end, type });
    }
  });

  return ends;
}

/**
 * Find all chains of length nfirst (> 1) separated from a second chain of length
 *      nsecond by nsep empty squares.
 * 
 *      ex. Let shape = { first: 3, second: 1, sep: 1 }
 * 
 *         Then ['1','1','1','.','1'] would be a 3,1-separated chain
 * 
 * chains is expected to have four dimensions:
 *    1. Player id - either 1 or 2
 *    2. Direction
 *    3. Chain size - integer between 1 and 7
 *    4. Chains - JSON encoded array of board coordinates
 * 
 * @param {Object} config Configuration object that defines the type of chains to collect
 */
export function getSeparatedChains({
  cells,
  chains,
  playerId,
  shape: {
    first,
    second,
    sep = 1
  }
}) {
  // Disallow certain chain queries
  if (first < 2 || first > 4 || second < 1 || second > 4) {
    return [];
  }

  const separatedChains = [];
  let iterationPts = [];

  // Get each chain of length first as well as its direction
  for (let direction in chains[playerId]) {
    const chainsOfLenFirst = chains[playerId][direction][first];
    // For each chain, add its chain ends to the set of points we will need to
    // start iterating from
    for (let chain of chainsOfLenFirst) {
      chain = JSON.parse(chain);
      direction = parseInt(direction);
      const ends = getChainEnds({ chain, direction, withEndMarkers: true });
      iterationPts = iterationPts.concat([{ direction, ends, chain }]);
    }
  }

  // Build the target array, which we will use to match the second part of the separated chain
  let targets = arrays.fill(butils.EMPTY, sep);
  targets = targets.concat(arrays.fill(playerId, second));
  targets.push(butils.EMPTY);

  iterationPts.forEach(({ direction, ends, chain }) => {
    ends.forEach(({ end, type }) => {
      let nMatched = 0;
      const isReversed = type === HEAD;

      const secondChainSquares = [
        ...butils.iterateFrom({
          cells,
          direction,
          n: targets.length,
          start: end,
          reversed: isReversed
        })
      ];

      if (secondChainSquares.length < targets.length - 1) {
        return;
      }

      // Check if squares match what we are expecting
      for (let i = 0; i < secondChainSquares.length; i++) {
        const { value } = secondChainSquares[i];
        if (value !== targets[i]) {
          break;
        }
        nMatched += 1;
      }

      if (nMatched === secondChainSquares.length) {
        separatedChains.push({ chain, direction, reversed: isReversed });
      }
    });
  });

  return separatedChains;
}

function getSquareInDirection({
  cells,
  start,
  direction,
  reversed = false
}) {
  const iter = butils.iterateFrom({ cells, direction, n: 2, start, reversed });
  iter.next();
  for (let square of iter) {
    return square;
  }
  return {};
}

function hasCrossingFromPoint({ cells, point, direction, reversed, matches }) {
  const perpDirection = direction === butils.HORIZONTAL
        ? butils.VERTICAL
        : butils.ANTI_DIAGONAL;
  
  let { coord } = getSquareInDirection({
    cells,
    start: point,
    direction: perpDirection,
    reversed: !reversed
  });

  let targets;

  if (coord == null) {
    coord = point;
    targets = matches;
  } else {
    targets = [butils.EMPTY, ...matches];
  }

  const crossingSquares = [
    ...butils.iterateFrom({
      cells,
      start: coord,
      direction: perpDirection,
      n: targets.length,
      reversed
    })
  ];

  if (crossingSquares.length < targets.length - 1) {
    return false;
  }

  let nMatched = 0;

  for (let i = 0; i < crossingSquares.length; i++) {
    const { value } = crossingSquares[i];
    if (value !== targets[i]) {
      break;
    }
    nMatched += 1;
  }

  return nMatched === crossingSquares.length;
}

/**
 * * chains is expected to have four dimensions:
 *    1. Player id - either 1 or 2
 *    2. Direction
 *    3. Chain size - integer between 1 and 7
 *    4. Chains - JSON encoded array of board coordinates
 */
export function findCrossings({
  cells,
  chains,
  playerId,
  size
}) {
  const crossings = [];

  const matches = arrays.fill(playerId, size);
  matches.push(butils.EMPTY);

  [butils.HORIZONTAL, butils.DIAGONAL].forEach(direction => {
    const chainsOfSize = chains[playerId][direction][size];
    chainsOfSize.forEach(chain => {
      chain = JSON.parse(chain);
      const head = chain[0];
      const tail = chain[chain.length-1];
      
      [head, tail].forEach(end => {
        [true, false].forEach(reversed => {
          const hasCrossing = hasCrossingFromPoint({
            cells,
            point: end,
            direction,
            reversed,
            matches
          });

          if (hasCrossing) {
            crossings.push({
              chain,
              direction,
              end,
              reversed
            });
          }
        });
      });
    });
  });

  return crossings;
};