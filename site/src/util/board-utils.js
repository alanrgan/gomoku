import { collect, getChainEnds } from './chain-utils';

// Directional constants for board iteration
export const HORIZONTAL = 0;
export const VERTICAL = 1;
export const DIAGONAL = 2;
export const ANTI_DIAGONAL = 3;

export const DIRECTIONS = [HORIZONTAL, VERTICAL, DIAGONAL, ANTI_DIAGONAL];

// Marker that indicates the end of an iteration block
export const BLOCK_END = 'BLOCK_END';

// Piece type constants
export const PLAYER_ONE = 1;
export const PLAYER_TWO = 2;
export const EMPTY = null;

export const directionToCmp = DIRECTIONS.reduce((acc, dir) => {
  const idx = (dir === HORIZONTAL || dir === ANTI_DIAGONAL) ? 1 : 0;
  acc[dir] = (a, b) => a[idx] - b[idx];
  return acc;
}, {});

/**
 * Iterate through the given cells along a particular direction.
 * Yields the value of the current cell and its corresponding coordinates.
 * 
 * If withEndMarker is set to true, then END_MARKER will be yielded
 * when the values along the specified direction have been exhausted.
 * 
 * @param {Array} cells 
 * @param {int} direction 
 * @param {boolean} withEndMarker 
 * 
 * @yields {Object or String}
 *  An object with keys 'value' and 'coord' that contain
 *  the value at the current cell and the corresponding coordinate.
 * 
 *  If withEndMarker is true, then a BLOCK_END constant will be yielded
 *  when all values along a particular direction have been exhausted.
 */
export function* iterate({
  cells,
  direction,
  withEndMarker = true,
}) {
  const rows = cells.length;
  const columns = cells[0].length;

  switch (direction) {
    case HORIZONTAL:
    case VERTICAL:
      const primaryDim = direction === VERTICAL ? columns : rows;
      const secondaryDim = direction === VERTICAL ? rows : columns;
      for (let i = 0; i < primaryDim; i++) {
        for (let j = 0; j < secondaryDim; j++) {
          yield (direction === VERTICAL)
            ? { value: cells[j][i], coord: [j, i] }
            : { value: cells[i][j], coord: [i, j] };
        }
        
        if (withEndMarker) {
          yield BLOCK_END;
        }
      }
      break;
    case DIAGONAL:
    case ANTI_DIAGONAL: {
      const isAntiDiag = direction === ANTI_DIAGONAL;
      const maxDim = Math.max(rows, columns);

      let nYielded;
      for (let k = 0; k <= 2 * (maxDim - 1); k++) {
        nYielded = 0;
        for (let y = rows - 1; y >= 0; y--) {
          const x = k - (isAntiDiag ? rows - y : y);
          if (x >= 0 && x < columns) {
            yield { value: cells[y][x], coord: [y,x] };
            nYielded += 1;
          }
        }

        if (withEndMarker && nYielded > 0) {
          yield BLOCK_END;
        }
      }
      break;
    }
    default:
      break;
  }
}

export function* iterateFrom({
  cells,
  direction,
  n,
  start = [0,0],
  reversed = false
}) {
  const iterDirs = {
    [HORIZONTAL]: [0, 1],
    [VERTICAL]: [1, 0],
    [DIAGONAL]: [-1, 1],
    [ANTI_DIAGONAL]: [1, 1]
  };

  let niters = 0;
  let [x, y] = start;
  let [dx, dy] = iterDirs[direction];

  if (reversed) {
    dx *= -1;
    dy *= -1;
  }

  while (isValidCell(cells, x, y) && (!n || niters < n)) {
    yield { value: cells[x][y], coord: [x, y] };
    x += dx;
    y += dy;
    niters += 1;
  }
}

export const isValidCell = (cells, x, y) => {
  const rows = cells.length;
  const columns = cells[0].length;
  
  return x >= 0 && x < rows && y >= 0 && y < columns;
}

export const isValidMove = (cells, x, y) => {
  return isValidCell(cells, x, y) && cells[x][y] === EMPTY;
}

export function* getEmptyCells({ cells, direction }) {
  const iter = iterate({ cells, direction, withEndMarker: false });
  for (let cell of iter) {
    if (cell.value === EMPTY) {
      yield cell;
    }
  }
}

export function getWinner(cells) {
  const chains = collect(cells);
  for (let player in chains) {
    player = parseInt(player);
    for (let dir in chains[player]) {
      dir = parseInt(dir);
      for (let i = 5; i < 8; i++) {
        if (chains[player][dir][i].size > 0) {
          return player;
        }
      }
    }
  }

  return null;
}

export function* getEndSquares({ cells, chains, playerId, chainSize }) {
  for (let direction of DIRECTIONS) {
    for (let chain of chains[playerId][direction][chainSize]) {
      chain = JSON.parse(chain);
      // Find chain ends
      const chainEnds = getChainEnds({ chain, direction })
        // Only consider valid moves
        .filter(([x,y]) => isValidCell(cells, x, y) && cells[x][y] === EMPTY);

      for (let ends of chainEnds) {
        yield ends.sort((a,b) => a[1] !== b[1]
          ? a[1] - b[1]
          : a[0] - b[0]
        );
      }
    }
  };
}

/**
 * Get all board cells c such that if the specified player sets a
 * piece at that cell, they would win.
 * 
 * For instance, in the following row, the '.' marks a winning coordinate for 'x'
 *  x x . x x
 * 
 * @param {{cells, playerId}}
 *  cells - board cells
 *  playerId - id of the player to get winning coords for
 */
export function getWinningCoords({ cells, playerId }) {
  const winningCoords = [];

  const opponentId = playerId === PLAYER_ONE ? PLAYER_TWO : PLAYER_ONE;

  DIRECTIONS.forEach(direction => {
    const iter = iterate({ cells, direction, withEndMarker: true });
    for (let cell of iter) {
      if (cell === BLOCK_END) {
        continue;
      }

      const { coord } = cell;
      
      // Iterate over the next five cells along the direction
      const fiveIter = iterateFrom({
        cells,
        direction,
        n: 5,
        start: coord
      });

      const block = [...fiveIter];
      if (block.length !== 5) {
        continue;
      }
      
      let nOwnPiece = 0;
      let emptyCoord;

      for (let { value, coord: nextCoord } of block) {
        if (value === opponentId) {
          break;
        } else if (value !== EMPTY) {
          nOwnPiece += 1;
        } else {
          emptyCoord = nextCoord;
        }
      }

      if (nOwnPiece === 4 && emptyCoord != null) {
        winningCoords.push(emptyCoord);
      }
    }
  });

  return winningCoords;
}