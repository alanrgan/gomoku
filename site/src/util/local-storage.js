export const STATE_ID = 'gomoku/STATE';

export const loadState = () => {
  try {
    const serialized = localStorage.getItem(STATE_ID);
    if (serialized === null) {
      return undefined;
    }
    return JSON.parse(serialized);
  } catch {}
};

export const saveState = state => {
  try {
    const { game } = state; 
    const serialized = JSON.stringify({ game });
    localStorage.setItem(STATE_ID, serialized);
  } catch {}
};