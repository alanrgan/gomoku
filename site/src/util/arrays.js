export function fill(item, count) {
  const arr = [];
  for (let i = 0; i < count; i++) {
    arr.push((typeof item) === 'function'
      ? item()
      : item
    );
  }

  return arr;
};