import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSageMiddleware from 'redux-saga';

import throttle from 'lodash/throttle';
import { loadState, saveState } from './util/local-storage';

import gomokuApp from './modules';
import rootSaga from './sagas';

const sagaMiddleware = createSageMiddleware();
const persistedState = loadState();

const store = createStore(
  gomokuApp,
  persistedState,
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

store.subscribe(throttle(() => {
  saveState(store.getState());
}, 300));

const theme = createMuiTheme({
  palette: {
    secondary: {
      main: '#058728',
      contrastText: '#fff'
    }
  }
});

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <App />
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
