## Build locally

Make sure all of the following project dependencies are met:

- Crystal v0.26.0 and shards
- yarn package manager
- Docker

## Installation

In the `site` directory, run `yarn install && yarn install -D`.

To start the site in development mode, run `yarn start`.

Open http://localhost:3000/ to view it in the browser.

In order to enable websockets for real-time multi-tenant gameplay, run `make local` under the `websockets` directory.