require "json"

class Message
  JSON.mapping(
    tag: String,
    body: String
  )
end

# Message types, parsed from Message.body

class Connect
  JSON.mapping(
    game_id: String,
    name: String
  )
end

class Create
  JSON.mapping(
    name: String
  )
end

class MakeMove
  JSON.mapping(
    player_id: UInt32,
    move: Array(UInt32)
  )
end