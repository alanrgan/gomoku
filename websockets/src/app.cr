require "kemal"
require "./handler"

alias SocketInfo = {name: String, socket: HTTP::WebSocket}
alias SocketHash = Hash(String, Array(SocketInfo))

sockets = [] of HTTP::WebSocket
game_hash = SocketHash.new

ws "/" do |socket|
  sockets << socket

  puts "A client connected"
  socket.send(%<"hello world">);
  handler = Handler.new socket, game_hash

  socket.on_message do |message|
    handler.dispatch message
  end

  socket.on_close do |_|
    handler.disconnect ignore_self: true
    sockets.delete(socket)
    puts "Closing socket: #{socket}"
  end
end

Kemal.run 5555