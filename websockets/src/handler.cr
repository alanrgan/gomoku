require "uuid"
require "./status_code"
require "./message"

class RequestFailException < Exception
end

class Handler
  @game_id : String?
  @player_name : String?

  def initialize(@socket : HTTP::WebSocket, @game_hash : SocketHash)
  end

  def fail(reason)
    self.report StatusCode::Error, error: reason
    raise RequestFailException.new
  end

  def report(code, message : String? = nil, **extras)
    self.report @socket, code, message, **extras
  end

  def report(socket, code, message : String? = nil, *, other_sockets = [] of HTTP::WebSocket, **extras)
    if socket.closed?
      return
    end

    payload = { status: code, payload: extras.merge ({ message: message }) }
    payload = payload.to_json
    socket.send payload

    other_sockets.each { |ws| ws.send payload }
  end

  # Takes as input a command encoded as a JSON string with
  # 'tag' and 'body' attributes.
  # 'tag' specifies the type of the command
  # 'body' specifies the command arguments and is itself a JSON encoded string
  #
  # Invalid JSON schemas fail with JSON::MappingError at any nesting level
  def dispatch(message : String)
    begin
      begin
        msg = Message.from_json(message)
        case msg.tag
        when "connect" then self.connect Connect.from_json(msg.body)
        when "create" then self.create Create.from_json(msg.body)
        when "rehydrate" then self.rehydrate Connect.from_json(msg.body)
        when "disconnect" then self.disconnect
        when "make_move" then self.make_move MakeMove.from_json(msg.body)
        else
          self.fail "Unknown message type"
        end
      rescue ex : JSON::MappingError
        self.fail "Invalid JSON schema detected; #{ex.message}"
      end
    rescue RequestFailException
    end
  end

  def broadcast(code, message : String? = nil, *, other_sockets = [] of HTTP::WebSocket, **extras)
    sockets = @game_hash[@game_id].map { |socket_info| socket_info[:socket] }
    sockets.concat other_sockets
    sockets.each do |socket|
      self.report socket, code, message, **extras
    end
  end

  private def add_to_game(name)
    if game_id = @game_id
      @player_name = name
      if !@game_hash.has_key?(game_id)
        @game_hash[game_id] = [] of SocketInfo
      end

      @game_hash[game_id] = @game_hash[game_id] << {name: name, socket: @socket}
    end
  end

  private def connect(request)
    if !@game_id.nil?
      self.fail "Already connected to game #{@game_id}"
    end

    current_players = @game_hash.fetch(request.game_id, nil)

    if !current_players
      self.fail "No game found with that ID"
    elsif current_players.size == 2
      self.fail "Cannot join Game '#{request.game_id}': Already full"
    end

    @game_id = request.game_id

    self.add_to_game request.name

    players = @game_hash[@game_id].map { |socket_info| socket_info[:name] }

    self.broadcast StatusCode::Connected,
                   players: players,
                   gameId: @game_id
  end

  private def create(request)
    if !@game_id.nil?
      self.fail "Already connected to game #{@game_id}"
    end

    @game_id = UUID.random.hexstring[0, 16]
    self.add_to_game request.name

    self.report StatusCode::Created, gameId: @game_id
  end
  
  private def make_move(request)
    self.broadcast StatusCode::PlayerMove,
                   player: request.player_id,
                   move: request.move
  end

  # Set the member variables of the handler
  def rehydrate(request)
    @player_name = request.name
    @game_id = request.game_id
  end

  def disconnect(*, ignore_self = false)
    if game_id = @game_id
      @game_hash[@game_id].reject! { |socket_info| socket_info[:socket] === @socket }
     
      other_sockets = ignore_self ? [] of HTTP::WebSocket : [@socket]

      self.broadcast StatusCode::Disconnected,
                     gameId: game_id,
                     player: @player_name,
                     other_sockets: other_sockets
      
      if @game_hash[@game_id].empty?
        @game_hash.delete @game_id
      end

      @game_id = nil
    end
  end
end