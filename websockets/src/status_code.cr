module StatusCode
  Connected = "GAME_CONNECTED"
  Error = "ERROR"
  Created = "CREATED"
  Disconnected = "DISCONNECTED"
  PlayerMove = "PLAYER_MOVE"
end